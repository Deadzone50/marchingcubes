#pragma once
#include <vector>
#include "Vector/Matrix.h"
#include "Vector/Vector.h"
#include "Drawing.h"

class Character
{
public:
	void Move(Vec3f V);
	void SetPosition(Vec3f V);
	void SetDirection(Vec3f Dir);
	Vec3f GetPosition();
	Vec3f GetDirection();

	void SetMesh(Drawable *Mesh);
	Drawable* GetMesh();
	Mat4f GetModelMatrix();

	Vec3f Pos_ = {0,0,0};
	Vec3f Up_ = {0,1,0};
	Vec3f Right_ = {0,0,1};
	Vec3f LookingDirection_ = {1,0,0};

	Vec2f Rotation_ = {0,0};
	Vec3f CurrentSpeed_ = {0,0,0};
	bool Falling_ = true;
	bool Debug_ = false;

	//Mat4f ModelMatrix_;
	Drawable *Drawable_;
};
