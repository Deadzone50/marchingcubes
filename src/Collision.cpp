#include "Collision.h"

int RayTriangleIntersection(Vec3f Origin, Vec3f Direction, Vec3f Triangle[3], float &tmax)
{
	Vec3f N = NORMALIZE(CROSS(Triangle[1]-Triangle[0], Triangle[2]-Triangle[0]));
	float Denominator = DOT(N, Direction);
	if(fabsf(Denominator) < 0.0001f)	//parallel
		return 0;

	float D = DOT(N, Triangle[0]);
	float t = -(DOT(N, Origin) - D) / Denominator;

	if(t < 0 || t >= tmax)	//behind or too far away
		return 0;

	Vec3f Cross;

	Vec3f P = Origin+t*Direction;
	Vec3f Edge0 = Triangle[1] - Triangle[0];	//check if point is inside the triangle
	Vec3f ToPoint0 = P - Triangle[0];
	Cross = CROSS(Edge0, ToPoint0);
	if(DOT(N, Cross) < 0)
		return 0;

	Vec3f Edge1 = Triangle[2] - Triangle[1];	//check if point is inside the triangle
	Vec3f ToPoint1 = P - Triangle[1];
	Cross = CROSS(Edge1, ToPoint1);
	if(DOT(N, Cross) < 0)
		return 0;

	Vec3f Edge2 = Triangle[0] - Triangle[2];	//check if point is inside the triangle
	Vec3f ToPoint2 = P - Triangle[2];
	Cross = CROSS(Edge2, ToPoint2);
	if(DOT(N, Cross) < 0)
		return 0;

	tmax = t;
	if(Denominator > 0)
		return -1;	//backside of triangle hit
	else
		return 1;
}

bool RayAABBIntersection(Vec3f Origin, Vec3f Direction, Vec3i AABB[2], float &tmax)
{
	//if(AABB[0].x <= Origin.x &&
	//   AABB[0].y <= Origin.y &&
	//   AABB[0].z <= Origin.z &&

	//   Origin.x <= AABB[1].x &&
	//   Origin.y <= AABB[1].y &&
	//   Origin.z <= AABB[1].z)
	//{

	//	return true;
	//}
	float Currentmin = (AABB[0].x - Origin.x)/Direction.x;
	float Currentmax = (AABB[1].x - Origin.x)/Direction.x;
	if(Currentmin > Currentmax)
		std::swap(Currentmin, Currentmax);

	float tymin = (AABB[0].y - Origin.y)/Direction.y;
	float tymax = (AABB[1].y - Origin.y)/Direction.y;
	if(tymin > tymax)
		std::swap(tymin, tymax);

	if(Currentmin > tymax || Currentmax < tymin)
		return false;

	if(tymin > Currentmin)
		Currentmin = tymin;

	if(tymax < Currentmax)
		Currentmax = tymax;

	float tzmin = (AABB[0].z - Origin.z)/Direction.z;
	float tzmax = (AABB[1].z - Origin.z)/Direction.z;
	if(tzmin > tzmax)
		std::swap(tzmin, tzmax);

	if(Currentmin > tzmax || Currentmax < tzmin)
		return false;

	if(tzmin > Currentmin)
		Currentmin = tzmin;

	//if(tzmax < Currentmax)
	//	Currentmax = tzmax;

	if(Currentmin <= tmax)
		return true;
	else
		return false;
}

Octree::Octree(uint8_t WorldSize)
{
	RootNode_.AABB[0] = {0,0,0};
	RootNode_.AABB[1] = {WorldSize, WorldSize, WorldSize};
	std::vector<OctNode*> ToProcess;
	ToProcess.push_back(&RootNode_);
	while(!ToProcess.empty())
	{
		OctNode *CurrentNode = ToProcess.back();
		ToProcess.pop_back();
		if((CurrentNode->AABB[1].x - CurrentNode->AABB[0].x) > 1)
		{
			int HalfStep = (CurrentNode->AABB[1].x - CurrentNode->AABB[0].x)/2;
			int RootX = CurrentNode->AABB[0].x;
			int RootY = CurrentNode->AABB[0].y;
			int RootZ = CurrentNode->AABB[0].z;
			for(int i = 0; i < 8; ++i)
			{
				OctNode *NewNode = new OctNode;
				NewNode->AABB[0].x = RootX + HalfStep*(i%2);
				NewNode->AABB[1].x = RootX + HalfStep*(i%2 +1);

				NewNode->AABB[0].y = RootY + HalfStep*((i/2)%2);
				NewNode->AABB[1].y = RootY + HalfStep*((i/2)%2 +1);

				NewNode->AABB[0].z = RootZ + HalfStep*(i/4);
				NewNode->AABB[1].z = RootZ + HalfStep*(i/4 +1);

				NewNode->Parent = CurrentNode;
				CurrentNode->Children[i] = NewNode;
				ToProcess.push_back(NewNode);
			}
		}
	}
}

void Octree::AddTriangle(uint32_t TriangeIndex[3], Vec3i CubeCorner0)
{
	std::vector<OctNode*> ToProcess;
	ToProcess.push_back(&RootNode_);
	while(!ToProcess.empty())
	{
		OctNode *CurrentNode = ToProcess.back();
		ToProcess.pop_back();
		if( CurrentNode->AABB[0].x <= CubeCorner0.x &&
			CurrentNode->AABB[0].y <= CubeCorner0.y &&
			CurrentNode->AABB[0].z <= CubeCorner0.z &&

			CubeCorner0.x+1 <= CurrentNode->AABB[1].x &&
			CubeCorner0.y+1 <= CurrentNode->AABB[1].y &&
			CubeCorner0.z+1 <= CurrentNode->AABB[1].z)
		{
			if(CurrentNode->Children[0])	//if not leaf node
			{
				for(int i = 0; i < 8; ++i)
					ToProcess.push_back(CurrentNode->Children[i]);
			}
			else
			{
				if( CurrentNode->AABB[0].x == CubeCorner0.x &&
					CurrentNode->AABB[0].y == CubeCorner0.y &&
					CurrentNode->AABB[0].z == CubeCorner0.z &&

					CubeCorner0.x+1 == CurrentNode->AABB[1].x &&
					CubeCorner0.y+1 == CurrentNode->AABB[1].y &&
					CubeCorner0.z+1 == CurrentNode->AABB[1].z)
				{
					CurrentNode->Triangles.push_back(TriangeIndex[0]);
					CurrentNode->Triangles.push_back(TriangeIndex[1]);
					CurrentNode->Triangles.push_back(TriangeIndex[2]);
					return;
				}
			}
		}
	}
}

std::vector<uint32_t> Octree::Intersect(Vec3f Origin, Vec3f Direction, float &tmax)
{
	std::vector<uint32_t> PossibleTriangles;

	std::vector<OctNode*> ToProcess;
	ToProcess.push_back(&RootNode_);
	while(!ToProcess.empty())
	{
		OctNode *CurrentNode = ToProcess.back();
		ToProcess.pop_back();
		if(RayAABBIntersection(Origin, Direction, CurrentNode->AABB, tmax))
		{
			if(CurrentNode->Children[0])	//if not leaf node
			{
				for(int i = 0; i < 8; ++i)
					ToProcess.push_back(CurrentNode->Children[i]);
			}
			else
			{
				PossibleTriangles.insert(PossibleTriangles.end(), CurrentNode->Triangles.begin(), CurrentNode->Triangles.end());
			}
		}
	}

	return PossibleTriangles;
}
