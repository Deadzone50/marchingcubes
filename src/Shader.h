#pragma once
#include <GL/glew.h>
#include <string>
#include "Vector/Matrix.h"

class Shader
{
public:
	Shader() {};
	Shader(const GLchar *VertexPath, const GLchar *FragmentPath, const GLchar *GeometryPath = NULL);

	void Use();

	void SetBool(const std::string &Name, bool Value) const;
	void SetInt(const std::string &Name, int Value) const;
	void SetFloat(const std::string &Name, float Value) const;
	void SetVec3(const std::string &Name, const Vec3f &Vec) const;
	void SetMat4(const std::string &Name, const Mat4f &Mat) const;

	unsigned int ID;
};
