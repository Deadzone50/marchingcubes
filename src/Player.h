#pragma once
#include <vector>
#include "Vector/Matrix.h"
#include "Vector/Vector.h"
#include "Character.h"
#include "Map.h"

class PlayerClass : public Character
{
public:
	void SetRotation(Vec2f Angles);
	void Rotate(Vec2f Angles);
	Mat4f GetViewMatrix();

	void Jump();

	void CheckCollisions(Map &World);
	void Step(float dt, Map &World);
};
