#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices=18) out;

uniform mat4 ShadowVP[6];

out vec4 FragPos;

void main()
{
	for(int face = 0; face < 6; ++face)
	{
		gl_Layer = face;	//what face to render to on the cubemap
		for(int i = 0; i < 3; ++i)	//triangle vertices
		{
			FragPos = gl_in[i].gl_Position;
			gl_Position = ShadowVP[face] * FragPos;	//transform to light view
			EmitVertex();
		}
		EndPrimitive();
	}
}
