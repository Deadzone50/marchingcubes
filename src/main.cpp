#include <GL/glew.h>
#include <GL/glut.h>
#include <Vector/Vector.h>
#include <Vector/Matrix.h>

#include <chrono>
#include <iostream>
#include <iomanip>		//std::setprecision
#include <random>

#include "Shader.h"
#include "Map.h"
#include "LSystem.h"
#include "Drawing.h"
#include "Character.h"
#include "Player.h"
#include "NPC.h"
#include "Collision.h"
#include "FileIO.h"

const unsigned int SHADOW_SIZE = 256;

const uint8_t Threshold = 128;

auto t0h = std::chrono::high_resolution_clock::now();

bool KeyStates[256] = {false};
bool KeySpecialStates[256] = {false};
bool KeyChanged[256] = {false};

bool InitMouse = false;
Vec2i MousePos, MouseDelta;
Vec3f MousePos3d, MouseDir;
bool MouseButton[5]{false};
bool MouseChanged[5] = {false};

Vec2i ScreenSize;
const uint8_t WORLDSIZE = 2;
const int NumLights = 2;
Map World(WORLDSIZE);
PlayerClass *Player = new PlayerClass();
NPC *Enemy = new NPC();

void KeyDown(unsigned char Key, int MouseX, int MouseY)
{
	(void)MouseX;
	(void)MouseY;
	KeyStates[Key] = true;
	KeyChanged[Key] = true;
}
void KeyUp(unsigned char Key, int MouseX, int MouseY)
{	
	(void)MouseX;
	(void)MouseY;
	KeyStates[Key] = false;
	if('a' <= Key && Key <= 'z')									//97-122
		KeyStates[Key-32] = false;
	else if('A' <= Key && Key <= 'Z')								//65-90
		KeyStates[Key+32] = false;
}
void KeySpecialDown(int Key, int MouseX, int MouseY)
{
	(void)MouseX;
	(void)MouseY;
	KeySpecialStates[Key] = true;
}
void KeySpecialUp(int Key, int MouseX, int MouseY)
{
	(void)MouseX;
	(void)MouseY;
	KeySpecialStates[Key] = false;
}
void MouseFunc(int button, int state, int x, int y)
{
	(void)x;
	(void)y;
	MouseChanged[button] = true;
	if(button == 3 || button == 4)	//mousewheel
	{
		if(state == GLUT_DOWN)
			MouseButton[button] = true;
	}
	else
	{
		if(state == GLUT_DOWN)								//left,right, middle buttons
			MouseButton[button] = true;
		else
			MouseButton[button] = false;
	}
}
void MousePassiveMove(int x, int y)
{
	MousePos = {x, ScreenSize.y - y};
	MouseDelta.x = x - ScreenSize.x/2;
	MouseDelta.y = ScreenSize.y - y - ScreenSize.y/2;
}
void Reshape(int width, int height)
{
	ScreenSize = {width, height};
	glViewport(0, 0, width, height);
}

#define GLUT_KEY_F1					0x0001
#define GLUT_KEY_F2					0x0002
#define GLUT_KEY_F3					0x0003
#define GLUT_KEY_F4					0x0004
#define GLUT_KEY_F5					0x0005
#define GLUT_KEY_F6					0x0006
#define GLUT_KEY_F7					0x0007
#define GLUT_KEY_F8					0x0008
#define GLUT_KEY_F9					0x0009
#define GLUT_KEY_F10				0x000A
#define GLUT_KEY_F11				0x000B
#define GLUT_KEY_F12				0x000C
#define GLUT_KEY_LEFT				0x0064
#define GLUT_KEY_UP					0x0065
#define GLUT_KEY_RIGHT				0x0066
#define GLUT_KEY_DOWN				0x0067
#define GLUT_KEY_PAGE_UP			0x0068
#define GLUT_KEY_PAGE_DOWN			0x0069
#define GLUT_KEY_HOME				0x006A
#define GLUT_KEY_END				0x006B
#define GLUT_KEY_INSERT				0x006C
#define GLUT_KEY_SHIFT				112
bool MouseInit_ = true;
bool WarpMouse = true;
void HandleMouse(float dt)
{
	if(MouseInit_)
	{
		MouseInit_ = false;
	}
	else
	{
		WarpMouse = false;
		if(MouseDelta.x || MouseDelta.y)
		{
			float MDx = (float)MouseDelta.x / 400;
			float MDy = (float)MouseDelta.y / 400;
			WarpMouse = true;
			Player->Rotate({MDy, MDx});
		}
	}

	if(WarpMouse)
	{
		glutWarpPointer(ScreenSize.x/2, ScreenSize.y/2);
	}
}

unsigned int VAO1, VBO1_Pos, VBO1_Col, VBO1_Norm, EBO1, VAOCorners;
unsigned int VAO2, VBO2_Pos, VBO2_Col, EBO2;
unsigned int DepthMapFBO, DepthCubeMap[8];
Shader NormalShader, SimpleShader, DepthShader;
unsigned int ModelLocation, ViewLocation, ProjectionLocation;
Mat4f ProjectionMatrix, ViewMatrix, ModelMatrix;

uint32_t Elements1, Elements2, CornerElements;
uint32_t CornersStart;
Drawable DWorld, DLights, DSelection, DBox;
DebugDrawing DDebug;

void UpdateSelection(int i)
{
	std::vector<Vec3f> VertexPos, VertexCol;
	std::vector<uint32_t> Indices;

	int x = i%(WORLDSIZE-1);
	int y = (i/(WORLDSIZE-1))%(WORLDSIZE-1);
	int z = i/((WORLDSIZE-1)*(WORLDSIZE-1));
	Vec3f B0 = World.Get(x,	y, z)<Threshold ? Vec3f{1,1,1} : Vec3f{1,0,0};
	Vec3f B1 = World.Get(x+1, y, z)<Threshold ? Vec3f{1,1,1} : Vec3f{1,0,0};
	Vec3f B2 = World.Get(x+1, y, z+1)<Threshold ? Vec3f{1,1,1} : Vec3f{1,0,0};
	Vec3f B3 = World.Get(x,	y, z+1)<Threshold ? Vec3f{1,1,1} : Vec3f{1,0,0};
	Vec3f T0 = World.Get(x,	y+1, z)<Threshold ? Vec3f{1,1,1} : Vec3f{1,0,0};
	Vec3f T1 = World.Get(x+1, y+1, z)<Threshold ? Vec3f{1,1,1} : Vec3f{1,0,0};
	Vec3f T2 = World.Get(x+1, y+1, z+1)<Threshold ? Vec3f{1,1,1} : Vec3f{1,0,0};
	Vec3f T3 = World.Get(x,	y+1, z+1)<Threshold ? Vec3f{1,1,1} : Vec3f{1,0,0};

	VertexCol.push_back(B0);
	VertexCol.push_back(B1);
	VertexCol.push_back(B2);
	VertexCol.push_back(B3);
	VertexCol.push_back(T0);
	VertexCol.push_back(T1);
	VertexCol.push_back(T2);
	VertexCol.push_back(T3);

	VertexPos.push_back({(float)x,	(float)y,	(float)z});
	VertexPos.push_back({(float)x+1,(float)y,	(float)z});
	VertexPos.push_back({(float)x+1,(float)y,	(float)z+1});
	VertexPos.push_back({(float)x,	(float)y,	(float)z+1});
	VertexPos.push_back({(float)x,	(float)y+1,	(float)z});
	VertexPos.push_back({(float)x+1,(float)y+1,	(float)z});
	VertexPos.push_back({(float)x+1,(float)y+1,	(float)z+1});
	VertexPos.push_back({(float)x,	(float)y+1,	(float)z+1});

	Indices.push_back(0);
	Indices.push_back(1);
	Indices.push_back(2);
	Indices.push_back(3);
	Indices.push_back(4);
	Indices.push_back(5);
	Indices.push_back(6);
	Indices.push_back(7);
	DSelection.SetBuffers(Indices, VertexPos, VertexCol);
}

void TestCube(bool Corners[8])
{
	std::vector<uint32_t> Indices;
	std::vector<Vec3f> VertexPos;
	std::vector<Vec3f> VertexCol;
	std::vector<Vec3f> VertexNorm;
	Vec3i CubeCorners[8] =
	{
		{0,0,0}, {1,0,0},
		{1,0,1}, {0,0,1},
		{0,1,0}, {1,1,0},
		{1,1,1}, {0,1,1}
	};
	World.Set(0,0,0, 255*Corners[0]);
	World.Set(1,0,0, 255*Corners[1]);
	World.Set(1,0,1, 255*Corners[2]);
	World.Set(0,0,1, 255*Corners[3]);

	World.Set(0,1,0, 255*Corners[4]);
	World.Set(1,1,0, 255*Corners[5]);
	World.Set(1,1,1, 255*Corners[6]);
	World.Set(0,1,1, 255*Corners[7]);

	World.MarchCube(Indices, VertexPos, VertexCol, VertexNorm, CubeCorners, 128);
	DWorld.SetBuffers(Indices, VertexPos, VertexCol, VertexNorm);
}

Vec3f LightPosOrig[8];
Vec3f LightColor[8] ={
	{1,1,1}, {1,0.1f,0.1f}, {0.1f,1,0.1f}, {0.1f,0.1f,1},
	{1,1,0.1f}, {0.1f,1,1}, {1,0.1f,1}, {0.5f,0.8f,0.3f},
};
Mat4f ShadowProjectionMatrix;
float LightFarPlane = 50.0f;
bool InitResources()
{
	for(int i = 0; i < 8; ++i)
	{
		//LightPosOrig[i] = {WORLDSIZE/2.0f-rand()%WORLDSIZE, WORLDSIZE/2.0f-rand()%WORLDSIZE, WORLDSIZE/2.0f-rand()%WORLDSIZE};
		LightPosOrig[i] = {WORLDSIZE/2.0f, WORLDSIZE-10, WORLDSIZE/2.0f};
		//float r = (rand()%101)/100.0f;
		//float g = (rand()%101)/100.0f;
		//float b = (rand()%101)/100.0f;
		//LightColor[i] = {r,g,b};
	}

	NormalShader = Shader("Shader/shader.vert", "Shader/shader.frag");
	SimpleShader = Shader("Shader/lampshader.vert", "Shader/lampshader.frag");
	DepthShader = Shader("Shader/depthshader.vert", "Shader/depthshader.frag", "Shader/depthshader.geom");

	DWorld = Drawable(GL_TRIANGLES, true, true);
	DLights = Drawable(GL_TRIANGLES, false, false);
	DSelection = Drawable(GL_POINTS, true, false);
	DDebug.Init();

	std::vector<Vec3f> VertexCol, VertexNorm;
	std::vector<uint32_t> WorldIndices;
	std::vector<Vec3f> WorldVertexPos;
	World.GetWorldVertexData(WorldIndices, WorldVertexPos, VertexCol, VertexNorm, Threshold);
	DWorld.SetBuffers(WorldIndices, WorldVertexPos, VertexCol, VertexNorm);

	World.m_NavMesh.Generate();
	for(auto &NZ : World.m_NavMesh.m_NavZones)
	{
		float r = (rand()%101)/100.0f;
		float g = (rand()%101)/100.0f;
		float b = (rand()%101)/100.0f;
		//for(uint32_t i = 0; i < NZ.m_Corners.size()-1; ++i)
		//{
		//	DDebug.DrawLine(NZ.m_Corners[i]+Vec3f{0,0.01f,0}, NZ.m_Corners[i+1]+Vec3f{0,0.01f,0}, {r,g,b});
		//}
		DDebug.DrawPolygon(NZ.m_Corners, {r,g,b});
	}
	DDebug.UpdateBuffers();

	World.AddEnemy(Enemy);
	World.AddPlayer(Player);

	Player->SetPosition({5, WORLDSIZE/2 +5, WORLDSIZE/2});
	Player->Debug_ = true;

	std::vector<uint32_t> Indices;
	std::vector<Vec3f> VertexPos;
	BoxMesh(0.7f, 2, 0.5f, {0,0,0}, Indices, VertexPos, VertexCol, VertexNorm);
	DBox = Drawable(GL_TRIANGLES, true, true);
	DBox.SetBuffers(Indices, VertexPos, VertexCol, VertexNorm);
	Enemy->SetPosition(Player->GetPosition() + Vec3f{3,0,0});
	Enemy->SetMesh(&DBox);
	Enemy->Debug_ = true;

	glPointSize(5);
	UpdateSelection(0);

	//Lamp
	Indices =
	{
		0,1,2,
		0,2,3,
		0,3,1
	};
	VertexPos =
	{
		{0,0,0},
		{-1,-1,1},
		{1,-1,1},
		{0,-1,-1}
	};
	DLights.SetBuffers(Indices, VertexPos);

	//shadow depth buffers
	glGenFramebuffers(1, &DepthMapFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, DepthMapFBO);

	glGenTextures(NumLights, &DepthCubeMap[0]);
	for(int i = 0; i < NumLights; ++i)
	{
		glBindTexture(GL_TEXTURE_CUBE_MAP, DepthCubeMap[i]);
		for(uint8_t i = 0; i < 6; ++i)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT,
						 SHADOW_SIZE, SHADOW_SIZE, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
		}
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	ShadowProjectionMatrix = Perspective(90, 1.0f, 1.0f, LightFarPlane);

	//ProjectionMatrix = Ortho(-1, 1, -1, 1, 0.01f, 10.0f);
	ProjectionMatrix = Perspective(90, (float)ScreenSize.x/ScreenSize.y, 0.01f, 1000.0f);

	
	return true;
}
bool Corners[8] = {false};
uint32_t DebugCube = 0;
GLenum CurrentDrawMode = GL_FILL;
void HandleKeys(float dt)
{
	float MoveSpeed_ = 30.0f;
	if(KeyStates['q'])
	{
		exit(0);
	}
	float Factor = 3.0f/2.0f;
	if(KeySpecialStates[GLUT_KEY_SHIFT])
	{
		Factor*=10;
	}
	if(KeyStates['w']||KeyStates['W'])
	{
		Player->Move(dt*MoveSpeed_*Factor*Vec3f{0,0,-1});
	}
	if(KeyStates['a']||KeyStates['A'])
	{
		Player->Move(dt*MoveSpeed_*Factor*Vec3f{-1,0,0});
	}
	if(KeyStates['s']||KeyStates['S'])
	{
		Player->Move(dt*MoveSpeed_*Factor*Vec3f{0,0,1});
	}
	if(KeyStates['d']||KeyStates['D'])
	{
		Player->Move(dt*MoveSpeed_*Factor*Vec3f{1,0,0});
	}

	{
		if(KeyChanged[' '] && KeyStates[' '])
		{
			Player->Jump();
			KeyChanged[' '] = false;
		}
		if(KeyChanged['z'] && KeyStates['z'])
		{
			Player->Debug_ = !Player->Debug_;
			KeyChanged['z'] = false;
		}
		if(KeyChanged['\t'] && KeyStates['\t'])
		{
			if(++DebugCube == (WORLDSIZE-1)*(WORLDSIZE-1)*(WORLDSIZE-1))
			{
				DebugCube = 0;
			}
			UpdateSelection(DebugCube);
			KeyChanged['\t'] = false;
		}
		if(KeyChanged['1'] && KeyStates['1'])
		{
			if(CurrentDrawMode == GL_FILL)
			{
				CurrentDrawMode = GL_LINE;
			}
			else
			{
				CurrentDrawMode = GL_FILL;
			}
			KeyChanged['1'] = false;
		}
		//if(KeyChanged['0'] && KeyStates['0']) {Corners[0] = !Corners[0]; KeyChanged['0'] = false;}
		//if(KeyChanged['1'] && KeyStates['1']) {Corners[1] = !Corners[1]; KeyChanged['1'] = false;}
		//if(KeyChanged['2'] && KeyStates['2']) {Corners[2] = !Corners[2]; KeyChanged['2'] = false;}
		//if(KeyChanged['3'] && KeyStates['3']) {Corners[3] = !Corners[3]; KeyChanged['3'] = false;}
		//if(KeyChanged['4'] && KeyStates['4']) {Corners[4] = !Corners[4]; KeyChanged['4'] = false;}
		//if(KeyChanged['5'] && KeyStates['5']) {Corners[5] = !Corners[5]; KeyChanged['5'] = false;}
		//if(KeyChanged['6'] && KeyStates['6']) {Corners[6] = !Corners[6]; KeyChanged['6'] = false;}
		//if(KeyChanged['7'] && KeyStates['7']) {Corners[7] = !Corners[7]; KeyChanged['7'] = false;}

		if(KeyChanged['+'] && KeyStates['+'])
		{
			uint8_t ic = Corners[0];
			ic |= (Corners[1] << 1);
			ic |= (Corners[2] << 2);
			ic |= (Corners[3] << 3);
			ic |= (Corners[4] << 4);
			ic |= (Corners[5] << 5);
			ic |= (Corners[6] << 6);
			ic |= (Corners[7] << 7);
			++ic;
			Corners[0] = (ic >> 0) & 1;
			Corners[1] = (ic >> 1) & 1;
			Corners[2] = (ic >> 2) & 1;
			Corners[3] = (ic >> 3) & 1;
			Corners[4] = (ic >> 4) & 1;
			Corners[5] = (ic >> 5) & 1;
			Corners[6] = (ic >> 6) & 1;
			Corners[7] = (ic >> 7) & 1;
			KeyChanged['+'] = false;
		}

		if(KeyChanged['-'] && KeyStates['-'])
		{
			uint8_t ic = Corners[0];
			ic |= (Corners[1] << 1);
			ic |= (Corners[2] << 2);
			ic |= (Corners[3] << 3);
			ic |= (Corners[4] << 4);
			ic |= (Corners[5] << 5);
			ic |= (Corners[6] << 6);
			ic |= (Corners[7] << 7);
			--ic;
			Corners[0] = (ic >> 0) & 1;
			Corners[1] = (ic >> 1) & 1;
			Corners[2] = (ic >> 2) & 1;
			Corners[3] = (ic >> 3) & 1;
			Corners[4] = (ic >> 4) & 1;
			Corners[5] = (ic >> 5) & 1;
			Corners[6] = (ic >> 6) & 1;
			Corners[7] = (ic >> 7) & 1;
			KeyChanged['-'] = false;
		}
		//TestCube(Corners);

		if(MouseChanged[0] && MouseButton[0])
		{
			float t = 3000;
			Vec3f P = Player->GetPosition()+Vec3f{0.001f,0.001f,0.001f};
			Vec3f Dir = NORMALIZE(Player->GetDirection());
			auto Intersect0 = std::chrono::high_resolution_clock::now();
			World.RayWorldInterSect(P, Dir, t);
			auto Intersect1 = std::chrono::high_resolution_clock::now();
			float Intersectdt = std::chrono::duration<float>(Intersect1 - Intersect0).count();
			std::cout << "Intersection ms: " << Intersectdt*1000;
			//DDebug.Clear();
			DDebug.DrawPoint(P+t*Dir, {0,0.8f,0});
			DDebug.DrawLine(P, P+t*Dir, {0,1,0});
			DDebug.UpdateBuffers();
			MouseChanged[0] = false;
		}
	}
}

float Time = 0;
uint8_t Frame = 0;
float dt;
void MainLoop()
{
	//uint32_t asd = rand()%500;
	//for(uint32_t i = 0; i < asd; ++i)
	//{
	//	Mat4f M1, M2;
	//	float a,b,c,d;
	//	a = (float)(rand()%256);
	//	b = (float)(rand()%256);
	//	c = (float)(rand()%256);
	//	d = (float)(rand()%256);
	//	M1.SetCol(0, {a,b,c,d});
	//	a = (float)(rand()%256);
	//	b = (float)(rand()%256);
	//	c = (float)(rand()%256);
	//	d = (float)(rand()%256);
	//	M1.SetCol(1, {a,b,c,d});
	//	a = (float)(rand()%256);
	//	b = (float)(rand()%256);
	//	c = (float)(rand()%256);
	//	d = (float)(rand()%256);
	//	M1.SetCol(2, {a,b,c,d});
	//	a = (float)(rand()%256);
	//	b = (float)(rand()%256);
	//	c = (float)(rand()%256);
	//	d = (float)(rand()%256);
	//	M1.SetCol(3, {a,b,c,d});

	//	a = (float)(rand()%256);
	//	b = (float)(rand()%256);
	//	c = (float)(rand()%256);
	//	d = (float)(rand()%256);
	//	M2.SetCol(0, {a,b,c,d});
	//	a = (float)(rand()%256);
	//	b = (float)(rand()%256);
	//	c = (float)(rand()%256);
	//	d = (float)(rand()%256);
	//	M2.SetCol(1, {a,b,c,d});
	//	a = (float)(rand()%256);
	//	b = (float)(rand()%256);
	//	c = (float)(rand()%256);
	//	d = (float)(rand()%256);
	//	M2.SetCol(2, {a,b,c,d});
	//	a = (float)(rand()%256);
	//	b = (float)(rand()%256);
	//	c = (float)(rand()%256);
	//	d = (float)(rand()%256);
	//	M2.SetCol(3, {a,b,c,d});
	//	Mat4f M3 = M1*M2;
	//}
	auto now = std::chrono::high_resolution_clock::now();
	dt = std::chrono::duration<float>(now - t0h).count();

	if(dt*1000 > 4)		//slow down reneding 4ms minimum
	{
		HandleMouse(dt);
		HandleKeys(dt);
		Player->Step(dt, World);
		Time+=dt;
		Vec3f LightPos[8];
		for(int i = 0; i < NumLights; ++i)
		{
			Mat3f MR;
			Vec3f Axle = NORMALIZE({float(i%2),1,float(i%3)});
			MR.Rotate(Axle, (1+i)*Time*0.3f + i*3.14f*2/8);
			LightPos[i] = MR*LightPosOrig[i] + Vec3f{WORLDSIZE/2,WORLDSIZE/2,WORLDSIZE/2};
		}

		glViewport(0, 0, SHADOW_SIZE, SHADOW_SIZE);
		glBindFramebuffer(GL_FRAMEBUFFER, DepthMapFBO);

		DepthShader.Use();
		DepthShader.SetFloat("FarPlane", LightFarPlane);
		ModelMatrix.Identity();
		DepthShader.SetMat4("Model", ModelMatrix);
		for(int i = 0; i < NumLights; ++i)
		{
			std::vector<Mat4f> ShadowVP;
			ShadowVP.push_back(ShadowProjectionMatrix*LookAt(LightPos[i], LightPos[i] + Vec3f{1,0,0}, {0,-1,0}));
			ShadowVP.push_back(ShadowProjectionMatrix*LookAt(LightPos[i], LightPos[i] + Vec3f{-1,0,0}, {0,-1,0}));
			ShadowVP.push_back(ShadowProjectionMatrix*LookAt(LightPos[i], LightPos[i] + Vec3f{0,1,0}, {0,0,1}));
			ShadowVP.push_back(ShadowProjectionMatrix*LookAt(LightPos[i], LightPos[i] + Vec3f{0,-1,0}, {0,0,-1}));
			ShadowVP.push_back(ShadowProjectionMatrix*LookAt(LightPos[i], LightPos[i] + Vec3f{0,0,1}, {0,-1,0}));
			ShadowVP.push_back(ShadowProjectionMatrix*LookAt(LightPos[i], LightPos[i] + Vec3f{0,0,-1}, {0,-1,0}));

			DepthShader.SetVec3("LightPos", LightPos[i]);
			for(int i = 0; i < 6; ++i)
			{
				DepthShader.SetMat4("ShadowVP["+std::to_string(i)+"]", ShadowVP[i]);
			}

			glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, DepthCubeMap[i], 0);
			glDrawBuffer(GL_NONE);
			glReadBuffer(GL_NONE);

			glClear(GL_DEPTH_BUFFER_BIT);
			DWorld.Draw();
		}
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, ScreenSize.x, ScreenSize.y);
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		NormalShader.Use();		//draw scene
		NormalShader.SetMat4("Projection", ProjectionMatrix);
		ModelMatrix.Identity();
		//ModelMatrix.Rotate({0,1,0}, Time);
		NormalShader.SetMat4("Model", ModelMatrix);

		//ViewMatrix.Identity();
		ViewMatrix = Player->GetViewMatrix();
		NormalShader.SetMat4("View", ViewMatrix);
		NormalShader.SetVec3("CameraPos", Player->GetPosition());
		NormalShader.SetFloat("LightFarPlane", LightFarPlane);

		NormalShader.SetInt("NumLights", NumLights);
		for(int i = 0; i < NumLights; ++i)
		{
			std::string LightString = "Lights["+std::to_string(i)+"]";
			NormalShader.SetVec3(LightString+".Pos", LightPos[i]);
			NormalShader.SetVec3(LightString+".Color", LightColor[i]);

			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(GL_TEXTURE_CUBE_MAP, DepthCubeMap[i]);
			NormalShader.SetInt("DepthMap["+std::to_string(i)+"]", i);
		}
	
		glPolygonMode(GL_FRONT_AND_BACK, CurrentDrawMode);	//draw corners
		DWorld.Draw();
		
		//Enemy->Step(dt, World);
		Drawable *D = Enemy->GetMesh();
		ModelMatrix = Enemy->GetModelMatrix();
		NormalShader.SetMat4("Model", ModelMatrix);
		D->Draw();

		ModelMatrix.Identity();
		NormalShader.SetMat4("Model", ModelMatrix);
		NormalShader.SetInt("NumLights", 0);
		DSelection.Draw();
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);	//draw corners
		DDebug.Draw();
		
		//glDrawElements(GL_TRIANGLES, CornerElements, GL_UNSIGNED_INT, (void*)(CornersStart*4));
		//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		SimpleShader.Use();
		SimpleShader.SetMat4("Projection", ProjectionMatrix);
		SimpleShader.SetMat4("View", ViewMatrix);
		for(int i = 0; i < NumLights; ++i)
		{
			ModelMatrix.Identity();
			ModelMatrix.Scale({0.5f, 0.8f, 0.5f});
			ModelMatrix.Translate(LightPos[i]);
			SimpleShader.SetMat4("Model", ModelMatrix);
			SimpleShader.SetVec3("Color", LightColor[i]);
			DLights.Draw();
		}

		glutSwapBuffers();
		if(!(++Frame%30))	//every 30th frame
		{
			std::cout << "ms: " << dt*1000;
			std::cout << "\tFPS: " << 1.0f/dt;
			std::cout << "\tPOS: " << Player->GetPosition();
			std::cout << "\tMPos: " << MousePos;
			std::cout << "\tMDelta: " << MouseDelta;
			std::cout << "\tDebugCube: " << DebugCube << "\n";
			std::cout << "\tDebugCorners: " 
				<< Corners[0] << "," << Corners[1] << "," << Corners[2] << "," << Corners[3] << ","
				<< Corners[4] << "," << Corners[5] << "," << Corners[6] << "," << Corners[7] << "\n";
			Frame = 0;
		}
		t0h = std::chrono::high_resolution_clock::now();
	}
}

int PauseExit(int Code)
{
	std::cin.ignore();
	return Code;
}

int main(int argc, char **argv)
{
	ScreenSize = Vec2i{800,600};

	
	ProjectionMatrix = Perspective(45.0f, (float)ScreenSize.x/ScreenSize.y, 0.1f, 100.0f);

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(ScreenSize.x, ScreenSize.y);
	glutInitWindowPosition(100,20);
	glutCreateWindow("MarchingCubes");

	glutDisplayFunc(MainLoop);
	glutReshapeFunc(Reshape);
	glutIdleFunc(MainLoop);

	glutKeyboardFunc(KeyDown);			glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(KeySpecialDown);	glutSpecialUpFunc(KeySpecialUp);
	glutMouseFunc(MouseFunc);
	glutMotionFunc(MousePassiveMove);
	glutPassiveMotionFunc(MousePassiveMove);
	glutIgnoreKeyRepeat(1);

	//glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_FRAMEBUFFER_SRGB);	//gamma correction
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glDepthFunc(GL_LESS);

	GLenum GlewStatus = glewInit();
	if(GlewStatus != GLEW_OK)
	{
		std::cout << "Error glewInit: " << glewGetErrorString(GlewStatus) << std::endl;
		return PauseExit(EXIT_FAILURE);
	}
	if (!GLEW_VERSION_3_3)
	{
		std::cout << "Error: your graphic card does not support OpenGL 3.3\n";
		return PauseExit(EXIT_FAILURE);
	}

	if (!InitResources())
	{
		std::cout << "Error: InitResources failed\n";
		return PauseExit(EXIT_FAILURE);
	}

	t0h = std::chrono::high_resolution_clock::now();
	glutMainLoop();

	return EXIT_SUCCESS;
}

