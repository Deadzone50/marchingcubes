#include "Matrix.h"

#include <sstream>
#include <iostream>
#include <cmath>

Mat4f LookAt(Vec3f CameraPos, Vec3f TargetPos, Vec3f GlobalUp)
{
	Vec3f ToCamera = NORMALIZE(CameraPos - TargetPos);
	Vec3f Right = NORMALIZE(CROSS(GlobalUp, ToCamera));
	Vec3f Up = CROSS(ToCamera, Right);

	Mat4f LAR, LAP;
	LAR.SetRow(0,{Right.x, Right.y, Right.z, 0});
	LAR.SetRow(1,{Up.x, Up.y, Up.z, 0});
	LAR.SetRow(2,{ToCamera.x, ToCamera.y, ToCamera.z, 0});

	LAP.SetCol(3, {-CameraPos.x, -CameraPos.y, -CameraPos.z, 1});

	return LAR*LAP;
}

Mat4f Perspective(float VerticalFOV, float AspectRatio, float NearField, float FarField)		//modeled after openGLs gluPerspective
{
	float FOVrad = VerticalFOV*3.1415926535897932384626f/360;
	float T = tanf(FOVrad)*NearField;
	float B = -T;
	float R = T*AspectRatio;
	float L = -R;
	float N = NearField;
	float F = FarField;
	//Vec4f R0 = {2*N/(R-L),		0,			0,				0};
	//Vec4f R1 = {0,				2*N/(T-B),	0,				0};
	//Vec4f R2 = {(R+L)/(R-L),		(T+B)/(T-B), -(F+N)/(F-N),	-1};
	//Vec4f R3 = {0,				0,			-2*F*N/(F-N),	0};

	Vec4f R0 = {2*N/(R-L),		0,			0,				0};
	Vec4f R1 = {0,				2*N/(T-B),	0,				0};
	Vec4f R2 = {0,				0,			 -(F+N)/(F-N),	-2*F*N/(F-N)};
	Vec4f R3 = {0,				0,			-1,				0};

	Mat4f Persp;
	Persp.SetRow(0, R0);
	Persp.SetRow(1, R1);
	Persp.SetRow(2, R2);
	Persp.SetRow(3, R3);
	return Persp;
}

Mat4f Ortho(float Left, float Right, float Bottom, float Top, float NearField, float FarField)
{
	float T = Top;
	float B = Bottom;
	float R = Right;
	float L = Left;
	float N = NearField;
	float F = FarField;
	//Vec4f R0 = {2*N/(R-L),		0,			0,				0};
	//Vec4f R1 = {0,				2*N/(T-B),	0,				0};
	//Vec4f R2 = {(R+L)/(R-L),		(T+B)/(T-B), -(F+N)/(F-N),	-1};
	//Vec4f R3 = {0,				0,			-2*F*N/(F-N),	0};

	Vec4f R0 = {2/(R-L),		0,			0,				-(R+L)/(R-L)};
	Vec4f R1 = {0,				2/(T-B),	0,				-(T+B)/(T-B)};
	Vec4f R2 = {0,				0,			 -2/(F-N),		-(F+N)/(F-N)};
	Vec4f R3 = {0,				0,			0,				1};

	Mat4f Ortho;
	Ortho.SetRow(0, R0);
	Ortho.SetRow(1, R1);
	Ortho.SetRow(2, R2);
	Ortho.SetRow(3, R3);
	
	return Ortho;
}

Mat4f INVERTreal(const Mat4f &M)
{
	Mat4f Inv;
	float inv[16], det;
	int i;

	inv[0] = M.m[5]  * M.m[10] * M.m[15] - 
		M.m[5]  * M.m[11] * M.m[14] - 
		M.m[9]  * M.m[6]  * M.m[15] + 
		M.m[9]  * M.m[7]  * M.m[14] +
		M.m[13] * M.m[6]  * M.m[11] - 
		M.m[13] * M.m[7]  * M.m[10];

	inv[4] = -M.m[4]  * M.m[10] * M.m[15] + 
		M.m[4]  * M.m[11] * M.m[14] + 
		M.m[8]  * M.m[6]  * M.m[15] - 
		M.m[8]  * M.m[7]  * M.m[14] - 
		M.m[12] * M.m[6]  * M.m[11] + 
		M.m[12] * M.m[7]  * M.m[10];

	inv[8] = M.m[4]  * M.m[9] * M.m[15] - 
		M.m[4]  * M.m[11] * M.m[13] - 
		M.m[8]  * M.m[5] * M.m[15] + 
		M.m[8]  * M.m[7] * M.m[13] + 
		M.m[12] * M.m[5] * M.m[11] - 
		M.m[12] * M.m[7] * M.m[9];

	inv[12] = -M.m[4]  * M.m[9] * M.m[14] + 
		M.m[4]  * M.m[10] * M.m[13] +
		M.m[8]  * M.m[5] * M.m[14] - 
		M.m[8]  * M.m[6] * M.m[13] - 
		M.m[12] * M.m[5] * M.m[10] + 
		M.m[12] * M.m[6] * M.m[9];

	inv[1] = -M.m[1]  * M.m[10] * M.m[15] + 
		M.m[1]  * M.m[11] * M.m[14] + 
		M.m[9]  * M.m[2] * M.m[15] - 
		M.m[9]  * M.m[3] * M.m[14] - 
		M.m[13] * M.m[2] * M.m[11] + 
		M.m[13] * M.m[3] * M.m[10];

	inv[5] = M.m[0]  * M.m[10] * M.m[15] - 
		M.m[0]  * M.m[11] * M.m[14] - 
		M.m[8]  * M.m[2] * M.m[15] + 
		M.m[8]  * M.m[3] * M.m[14] + 
		M.m[12] * M.m[2] * M.m[11] - 
		M.m[12] * M.m[3] * M.m[10];

	inv[9] = -M.m[0]  * M.m[9] * M.m[15] + 
		M.m[0]  * M.m[11] * M.m[13] + 
		M.m[8]  * M.m[1] * M.m[15] - 
		M.m[8]  * M.m[3] * M.m[13] - 
		M.m[12] * M.m[1] * M.m[11] + 
		M.m[12] * M.m[3] * M.m[9];

	inv[13] = M.m[0]  * M.m[9] * M.m[14] - 
		M.m[0]  * M.m[10] * M.m[13] - 
		M.m[8]  * M.m[1] * M.m[14] + 
		M.m[8]  * M.m[2] * M.m[13] + 
		M.m[12] * M.m[1] * M.m[10] - 
		M.m[12] * M.m[2] * M.m[9];

	inv[2] = M.m[1]  * M.m[6] * M.m[15] - 
		M.m[1]  * M.m[7] * M.m[14] - 
		M.m[5]  * M.m[2] * M.m[15] + 
		M.m[5]  * M.m[3] * M.m[14] + 
		M.m[13] * M.m[2] * M.m[7] - 
		M.m[13] * M.m[3] * M.m[6];

	inv[6] = -M.m[0]  * M.m[6] * M.m[15] + 
		M.m[0]  * M.m[7] * M.m[14] + 
		M.m[4]  * M.m[2] * M.m[15] - 
		M.m[4]  * M.m[3] * M.m[14] - 
		M.m[12] * M.m[2] * M.m[7] + 
		M.m[12] * M.m[3] * M.m[6];

	inv[10] = M.m[0]  * M.m[5] * M.m[15] - 
		M.m[0]  * M.m[7] * M.m[13] - 
		M.m[4]  * M.m[1] * M.m[15] + 
		M.m[4]  * M.m[3] * M.m[13] + 
		M.m[12] * M.m[1] * M.m[7] - 
		M.m[12] * M.m[3] * M.m[5];

	inv[14] = -M.m[0]  * M.m[5] * M.m[14] + 
		M.m[0]  * M.m[6] * M.m[13] + 
		M.m[4]  * M.m[1] * M.m[14] - 
		M.m[4]  * M.m[2] * M.m[13] - 
		M.m[12] * M.m[1] * M.m[6] + 
		M.m[12] * M.m[2] * M.m[5];

	inv[3] = -M.m[1] * M.m[6] * M.m[11] + 
		M.m[1] * M.m[7] * M.m[10] + 
		M.m[5] * M.m[2] * M.m[11] - 
		M.m[5] * M.m[3] * M.m[10] - 
		M.m[9] * M.m[2] * M.m[7] + 
		M.m[9] * M.m[3] * M.m[6];

	inv[7] = M.m[0] * M.m[6] * M.m[11] - 
		M.m[0] * M.m[7] * M.m[10] - 
		M.m[4] * M.m[2] * M.m[11] + 
		M.m[4] * M.m[3] * M.m[10] + 
		M.m[8] * M.m[2] * M.m[7] - 
		M.m[8] * M.m[3] * M.m[6];

	inv[11] = -M.m[0] * M.m[5] * M.m[11] + 
		M.m[0] * M.m[7] * M.m[9] + 
		M.m[4] * M.m[1] * M.m[11] - 
		M.m[4] * M.m[3] * M.m[9] - 
		M.m[8] * M.m[1] * M.m[7] + 
		M.m[8] * M.m[3] * M.m[5];

	inv[15] = M.m[0] * M.m[5] * M.m[10] - 
		M.m[0] * M.m[6] * M.m[9] - 
		M.m[4] * M.m[1] * M.m[10] + 
		M.m[4] * M.m[2] * M.m[9] + 
		M.m[8] * M.m[1] * M.m[6] - 
		M.m[8] * M.m[2] * M.m[5];

	det = M.m[0] * inv[0] + M.m[1] * inv[4] + M.m[2] * inv[8] + M.m[3] * inv[12];

	//if (det == 0)
	//	return false;

	det = 1.0f / det;

	for (i = 0; i < 16; i++)
		Inv.m[i] = inv[i] * det;

	//return true;
	return Inv;
}

Mat4f INVERTaffine(const Mat4f &M)		//inverses the transformation matrix without doing heavy computing, works for affine transforms
{
	Mat4f Inv;
	Inv.SetCol(0, Vec4f{M.m[0], M.m[1], M.m[2], 0});		//3x3 rotation matrix inverted (transposed)
	Inv.SetCol(1, Vec4f{M.m[4], M.m[5], M.m[6], 0});
	Inv.SetCol(2, Vec4f{M.m[8], M.m[9], M.m[10], 0});
	Inv.SetCol(3, Vec4f{-M.m[3], -M.m[7], -M.m[11], 1});	//inverse transpose
	return Inv;
}

Mat4f TRANSPOSE(const Mat4f &M)
{
	Mat4f T;
	T.SetCol(0, Vec4f{M.m[0], M.m[1], M.m[2], M.m[3]});
	T.SetCol(1, Vec4f{M.m[4], M.m[5], M.m[6], M.m[7]});
	T.SetCol(2, Vec4f{M.m[8], M.m[9], M.m[10], M.m[11]});
	T.SetCol(3, Vec4f{M.m[12], M.m[13], M.m[14], M.m[15]});
	return T;
}

std::ostream& operator<<(std::ostream& os, const Mat4f& M)
{
  os << M.m[0] << "," << M.m[1] << "," << M.m[2] << "," << M.m[3] << std::endl;
  os << M.m[4] << "," << M.m[5] << "," << M.m[6] << "," << M.m[7] << std::endl;
  os << M.m[8] << "," << M.m[9] << "," << M.m[10] << "," << M.m[11] << std::endl;
  os << M.m[12] << "," << M.m[13] << "," << M.m[14] << "," << M.m[15] << std::endl;
  return os;
}

Mat4f operator*(const Mat4f &M1, const Mat4f &M2)
{
	Mat4f M;
	for(int R = 0; R < 4;++R)
		for(int C = 0; C < 4;++C)
		{
			M.m[R*4 + C] = M1.m[R*4 + 0]*M2.m[0 + C] + M1.m[R*4 + 1]*M2.m[4 + C] + M1.m[R*4 + 2]*M2.m[8 + C] + M1.m[R*4 + 3]*M2.m[12 + C];
		}
	return M;
}

//Mat4f operator*(const Mat4f &M1, const Mat4f &M2)
//{
//	__m128 R1[4], C2[4];
//	R1[0] = _mm_set_ps(M1.m[3+0],	M1.m[2+0],	M1.m[1+0],	M1.m[0+0]);
//	R1[1] = _mm_set_ps(M1.m[3+4],	M1.m[2+4],	M1.m[1+4],	M1.m[0+4]);
//	R1[2] = _mm_set_ps(M1.m[3+8],	M1.m[2+8],	M1.m[1+8],	M1.m[0+8]);
//	R1[3] = _mm_set_ps(M1.m[3+12],	M1.m[2+12],	M1.m[1+12],	M1.m[0+12]);
//
//	C2[0] = _mm_set_ps(M2.m[0+12],	M2.m[0+8],	M2.m[0+4],	M2.m[0+0]);
//	C2[1] = _mm_set_ps(M2.m[1+12],	M2.m[1+8],	M2.m[1+4],	M2.m[1+0]);
//	C2[2] = _mm_set_ps(M2.m[2+12],	M2.m[2+8],	M2.m[2+4],	M2.m[2+0]);
//	C2[3] = _mm_set_ps(M2.m[3+12],	M2.m[3+8],	M2.m[3+4],	M2.m[3+0]);
//
//	Mat4f M;
//	for(int i = 0; i < 4; ++i)
//	{
//		__m128 v0 = _mm_mul_ps(R1[i], C2[0]);
//		__m128 v1 = _mm_mul_ps(R1[i], C2[1]);
//		__m128 v2 = _mm_mul_ps(R1[i], C2[2]);
//		__m128 v3 = _mm_mul_ps(R1[i], C2[3]);
//
//		__m128 tmp1 = _mm_hadd_ps(v0, v1);			//v0, v0, v1, v1
//		__m128 tmp2 = _mm_hadd_ps(v2, v3);			//v2, v2, v3, v3
//		__m128 R = _mm_hadd_ps(tmp1, tmp2);			//v0, v1, v2, v3
//	
//		M.SetRow(i, {R.m128_f32[0], R.m128_f32[1], R.m128_f32[2], R.m128_f32[3]});
//	}
//
//	return M;
//}

void
Mat4f::Identity()
{
	SetRow(0,Vec4f{1,0,0,0});
	SetRow(1,Vec4f{0,1,0,0});
	SetRow(2,Vec4f{0,0,1,0});
	SetRow(3,Vec4f{0,0,0,1});
}

void Mat4f::SetCol(int C, Vec4f Col)
{
	m[0+C] = Col.x;
	m[4+C] = Col.y;
	m[8+C] = Col.z;
	m[12+C] = Col.w;
}
void Mat4f::SetRow(int R, Vec4f Row)
{
	m[R*4+0] = Row.x;
	m[R*4+1] = Row.y;
	m[R*4+2] = Row.z;
	m[R*4+3] = Row.w;
}

Vec4f Mat4f::GetCol(int C) const
{
	Vec4f Result;
	Result.x = m[0+C];
	Result.y = m[4+C];
	Result.z = m[8+C];
	Result.w = m[12+C];
	return Result;
}

Vec4f Mat4f::GetRow(int R) const
{
	Vec4f Result;
	Result.x = m[R*4+0];
	Result.y = m[R*4+1];
	Result.z = m[R*4+2];
	Result.w = m[R*4+3];
	return Result;
}

Mat4f Mat4f::Rotate(const Vec3f &R)
{
	Mat4f Rot;
	Mat4f Rotated = *this;
	Rot.SetRow(0,Vec4f{1,0,0,0});					//x-axis
	Rot.SetRow(1,Vec4f{0,cosf(R.x),sinf(R.x),0});
	Rot.SetRow(2,Vec4f{0,-sinf(R.x),cosf(R.x),0});
	Rot.SetRow(3,Vec4f{0,0,0,1});
	Rotated = Rot * Rotated; 
	
	//std::cout << Rotated << std::endl;
	
	Rot.SetRow(0,Vec4f{cosf(R.y),0,-sinf(R.y),0});	//y-axis
	Rot.SetRow(1,Vec4f{0,1,0,0});
	Rot.SetRow(2,Vec4f{sinf(R.y),0,cosf(R.y),0});
	Rot.SetRow(3,Vec4f{0,0,0,1});
	Rotated = Rot * Rotated;
	
	//std::cout << Rotated << std::endl;
	
	Rot.SetRow(0,Vec4f{cosf(R.z),sinf(R.z),0,0});	//z-axis
	Rot.SetRow(1,Vec4f{-sinf(R.z),cosf(R.z),0,0});
	Rot.SetRow(2,Vec4f{0,0,1,0});
	Rot.SetRow(3,Vec4f{0,0,0,1});
	Rotated = Rot * Rotated;
	
	//std::cout << Rotated << std::endl;
	
	for (int i = 0; i < 16; ++i)
	{
		m[i] = Rotated.m[i];
	}
	return *this;
}

Mat4f Mat4f::SetRotation(const Vec3f &R)
{
	Mat3f Rot;
	Rot.SetRotation(R);
	for (int y = 0; y < 3; ++y)
	{
		for (int x = 0; x < 3; ++x)
		{
			m[4*y + x] = Rot.m[3*y + x];
		}
	}
	return *this;
}

Mat3f Mat3f::SetRotation(const Vec3f &R)
{
	Mat3f Rot;
	Mat3f Rotated;
	Rot.SetRow(0, Vec3f{1,0,0});					//x-axis
	Rot.SetRow(1, Vec3f{0,cosf(R.x),sinf(R.x)});
	Rot.SetRow(2, Vec3f{0,-sinf(R.x),cosf(R.x)});
	Rotated = Rot * Rotated;

	//std::cout << Rotated << std::endl;

	Rot.SetRow(0, Vec3f{cosf(R.y),0,-sinf(R.y)});	//y-axis
	Rot.SetRow(1, Vec3f{0,1,0});
	Rot.SetRow(2, Vec3f{sinf(R.y),0,cosf(R.y)});
	Rotated = Rot * Rotated;

	//std::cout << Rotated << std::endl;

	Rot.SetRow(0, Vec3f{cosf(R.z),sinf(R.z),0});		//z-axis
	Rot.SetRow(1, Vec3f{-sinf(R.z),cosf(R.z),0});
	Rot.SetRow(2, Vec3f{0,0,1});
	Rotated = Rot * Rotated;

	for (int y = 0; y < 3; ++y)
	{
		for (int x = 0; x < 3; ++x)
		{
			m[3*y + x] = Rotated.m[3*y + x];
		}
	}
	return *this;
}

Mat4f Mat4f::Rotate(const Vec3f &U, float Angle)
{
	Mat4f Rot;

	Mat3f R;
	R.Rotate(U,Angle);
	Rot.SetRow(0,{R.GetRow(0),0});
	Rot.SetRow(1,{R.GetRow(1),0});
	Rot.SetRow(2,{R.GetRow(2),0});
	Rot.SetRow(3,{0,0,0,1});

	*this = Rot* *this;

	return *this;
}

Mat4f Mat4f::Translate(const Vec3f &T)
{
	m[0+3] += T.x;
	m[4+3] += T.y;
	m[8+3] += T.z;
	return *this;
}

Mat4f Mat4f::SetPosition(const Vec3f & T)
{
	m[0+3] = T.x;
	m[4+3] = T.y;
	m[8+3] = T.z;
	return *this;
}

Mat4f Mat4f::Scale(const Vec3f &V)
{
	Mat4f S;
	S.m[0] = V.x;
	S.m[5] = V.y;
	S.m[10] = V.z;
	*this = S * *this;
	return *this;
}

//Mat3f Mat4f::GetXYZ()
//{
//	Mat3f Result;
//	for(int i = 0; i < 3; ++i)
//	{
//		Result.SetCol(i, GetCol(i).GetXYZ());
//	}
//	return Result;
//}

std::ostream& operator<<(std::ostream& os, const Mat3f& M)
{
	os << M.m[0] << "," << M.m[1] << "," << M.m[2] << std::endl;
	os << M.m[3] << "," << M.m[4] << "," << M.m[5] << std::endl;
	os << M.m[6] << "," << M.m[7] << "," << M.m[8] << std::endl;
	return os;
}

Mat3f INVERTortho(const Mat3f &M)		//inverses the transformation matrix without doing heavy computing, works for affine transforms
{
	Mat3f Inv;
	Inv.SetCol(0, Vec3f{M.m[0], M.m[1], M.m[2]});		//3x3 rotation matrix inverted (transposed)
	Inv.SetCol(1, Vec3f{M.m[3], M.m[4], M.m[5]});
	Inv.SetCol(2, Vec3f{M.m[6], M.m[7], M.m[8]});
	return Inv;
}

Mat3f TRANSPOSE(const Mat3f &M)
{
	Mat3f T;
	T.SetCol(0, Vec3f{M.m[0], M.m[1], M.m[2]});		//3x3 rotation matrix inverted (transposed)
	T.SetCol(1, Vec3f{M.m[3], M.m[4], M.m[5]});
	T.SetCol(2, Vec3f{M.m[6], M.m[7], M.m[8]});
	return T;
}

void Mat3f::SetCol(int C, Vec3f Col)
{
	m[0+C] = Col.x;
	m[3+C] = Col.y;
	m[6+C] = Col.z;
}
void Mat3f::SetRow(int R, Vec3f Row)
{
	m[R*3+0] = Row.x;
	m[R*3+1] = Row.y;
	m[R*3+2] = Row.z;
}

Vec3f Mat3f::GetCol(int C)
{
	Vec3f Result;
	Result.x = m[0+C];
	Result.y = m[3+C];
	Result.z = m[6+C];
	return Result;
}

Vec3f Mat3f::GetRow(int R)
{
	Vec3f Result;
	Result.x = m[R*3+0];
	Result.y = m[R*3+1];
	Result.z = m[R*3+2];
	return Result;
}

Mat3f Mat3f::Rotate(const Vec3f& R)
{
	Mat3f Rot;
	Mat3f Rotated = *this;
	Rot.SetRow(0, Vec3f{1,0,0});					//x-axis
	Rot.SetRow(1, Vec3f{0,cosf(R.x),sinf(R.x)});
	Rot.SetRow(2, Vec3f{0,-sinf(R.x),cosf(R.x)});
	Rotated = Rot * Rotated;

	//std::cout << Rotated << std::endl;

	Rot.SetRow(0, Vec3f{cosf(R.y),0,-sinf(R.y)});	//y-axis
	Rot.SetRow(1, Vec3f{0,1,0});
	Rot.SetRow(2, Vec3f{sinf(R.y),0,cosf(R.y)});
	Rotated = Rot * Rotated;

	//std::cout << Rotated << std::endl;

	Rot.SetRow(0, Vec3f{cosf(R.z),sinf(R.z),0});		//z-axis
	Rot.SetRow(1, Vec3f{-sinf(R.z),cosf(R.z),0});
	Rot.SetRow(2, Vec3f{0,0,1});
	Rotated = Rot * Rotated;

	//std::cout << Rotated << std::endl;

	for(int i = 0; i < 9; ++i)
	{
		m[i] = Rotated.m[i];
	}
	return *this;
}
Mat3f Mat3f::Rotate(const Vec3f & U, float Angle)
{
	Mat3f Rot;
	float Cos = cosf(Angle);
	float Sin = sinf(Angle);
	Rot.SetRow(0, {Cos+powf(U.x,2)*(1-Cos),		U.x*U.y*(1-Cos)-U.z*Sin,	U.x*U.z*(1-Cos)+U.y*Sin});
	Rot.SetRow(1, {U.y*U.x*(1-Cos)+U.z*Sin,		Cos+powf(U.y,2)*(1-Cos),	U.y*U.z*(1-Cos)-U.x*Sin});
	Rot.SetRow(2, {U.z*U.x*(1-Cos)-U.y*Sin,		U.z*U.y*(1-Cos)+U.x*Sin,	Cos+powf(U.z,2)*(1-Cos)});

	*this = Rot * *this;
	return *this;
}
void
Mat3f::Identity()
{
	SetRow(0, Vec3f{1,0,0});
	SetRow(1, Vec3f{0,1,0});
	SetRow(2, Vec3f{0,0,1});
}

Mat3f operator*(const Mat3f &M1, const Mat3f &M2)
{
	Mat3f M;
	for(int R = 0; R < 3; ++R)
		for(int C = 0; C < 3; ++C)
		{
			M.m[R*3 + C] = M1.m[R*3 + 0]*M2.m[0 + C] + M1.m[R*3 + 1]*M2.m[3 + C] + M1.m[R*3 + 2]*M2.m[6 + C];
		}
	return M;
}

Vec3f operator*(const Mat3f& M, const Vec3f& V)
{
	Vec3f Vec;
	Vec.x = M.m[0]*V.x + M.m[1]*V.y + M.m[2]*V.z;
	Vec.y = M.m[3]*V.x + M.m[4]*V.y + M.m[5]*V.z;
	Vec.z = M.m[6]*V.x + M.m[7]*V.y + M.m[8]*V.z;
	return Vec;
}
Vec4f operator*(const Mat4f& M, const Vec4f& V)
{
	Vec4f Vec;
	Vec.x = M.m[0]*V.x + M.m[1]*V.y + M.m[2]*V.z + M.m[3]*V.w;
	Vec.y = M.m[4]*V.x + M.m[5]*V.y + M.m[6]*V.z + M.m[7]*V.w;
	Vec.z = M.m[8]*V.x + M.m[9]*V.y + M.m[10]*V.z + M.m[11]*V.w;
	Vec.w = M.m[12]*V.x + M.m[13]*V.y + M.m[14]*V.z + M.m[15]*V.w;
	return Vec;
}

Mat4f operator*(float f, const Mat4f& M)
{
	Mat4f F;
	for(int i = 0; i < 16; ++i)
		F.m[i] = f*M.m[i];
	return F;
}

Mat4f operator*(const Mat4f& M, float f)
{
	return f*M;
}

Mat3f operator*(float f, const Mat3f& M)
{
	Mat3f F;
	for(int i = 0; i < 9; ++i)
		F.m[i] = f*M.m[i];
	return F;
}

Mat3f operator*(const Mat3f& M, float f)
{
	return f*M;
}