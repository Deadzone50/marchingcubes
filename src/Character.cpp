#include "Character.h"
#include "Collision.h"

void Character::Move(Vec3f V)
{
	if(Debug_)
	{
		Pos_ += V.x*Right_ + V.y*Up_ -V.z*LookingDirection_;
	}
	else
	{
		Vec3f PlaneForward = {LookingDirection_.x, 0, LookingDirection_.z};
		Pos_ += V.x*Right_ -V.z*PlaneForward;
	}
}

void Character::SetPosition(Vec3f V)
{
	Pos_ = V;
}

void Character::SetDirection(Vec3f Dir)
{
	LookingDirection_ = NORMALIZE(Dir);

	Rotation_.x = asin(LookingDirection_.y);
	Rotation_.y = acos(LookingDirection_.x/cos(Rotation_.x));
	if(LookingDirection_.z < 0)
		Rotation_.y = -Rotation_.y;

	Right_ = NORMALIZE(CROSS(LookingDirection_, { 0,1,0 }));
	Up_ = NORMALIZE(CROSS(Right_, LookingDirection_));
}

Vec3f Character::GetPosition() { return Pos_; }
Vec3f Character::GetDirection() { return LookingDirection_; }

void Character::SetMesh(Drawable *Mesh)
{
	Drawable_ = Mesh;
}

Drawable* Character::GetMesh()
{
	return Drawable_;
}

Mat4f Character::GetModelMatrix()
{
	Mat4f ModelMatrix;
	ModelMatrix.SetPosition(Pos_);
	ModelMatrix.SetCol(0, {-Right_, 0});
	ModelMatrix.SetCol(1, {Up_, 0});
	ModelMatrix.SetCol(2, {LookingDirection_, 0});
	return ModelMatrix;
}
