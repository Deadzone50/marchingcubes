#include <random>
#include <iostream>
#include <chrono>
//#include <algorithm>
#include "Map.h"
#include "CubeTable.h"
#include "LSystem.h"

NoiseTable::NoiseTable(uint32_t Size)
{
	m_NoiseTable.resize(Size*Size*Size);
	m_Size = Size;
	for(uint32_t z = 0; z < Size; ++z)
		for(uint32_t y = 0; y < Size; ++y)
			for(uint32_t x = 0; x < Size; ++x)
			{
				m_NoiseTable[z*Size*Size + y*Size + x] = rand()%256;
			}
}

uint8_t NoiseTable::TrilinearSample(uint32_t x, uint32_t y, uint32_t z, uint8_t Division)
{
	uint32_t x_fract = x%Division;
	uint32_t y_fract = y%Division;
	uint32_t z_fract = z%Division;

	uint32_t x_0 = x/Division;
	uint32_t x_1 = x/Division + 1;

	uint8_t V0xs = m_NoiseTable[(z/Division)*m_Size*m_Size + (y/Division)*m_Size + x_0];
	uint8_t V0xe = m_NoiseTable[(z/Division)*m_Size*m_Size + (y/Division)*m_Size + x_1];

	uint8_t V1xs = m_NoiseTable[(z/Division)*m_Size*m_Size + (y/Division +1)*m_Size + x_0];
	uint8_t V1xe = m_NoiseTable[(z/Division)*m_Size*m_Size + (y/Division +1)*m_Size + x_1];

	uint8_t V2xs = m_NoiseTable[(z/Division +1)*m_Size*m_Size + (y/Division)*m_Size + x_0];
	uint8_t V2xe = m_NoiseTable[(z/Division +1)*m_Size*m_Size + (y/Division)*m_Size + x_1];

	uint8_t V3xs = m_NoiseTable[(z/Division +1)*m_Size*m_Size + (y/Division +1)*m_Size + x_0];
	uint8_t V3xe = m_NoiseTable[(z/Division +1)*m_Size*m_Size + (y/Division +1)*m_Size + x_1];


	uint8_t VX0 = (V0xs*(Division-x_fract) + V0xe*(x_fract)) / Division;
	uint8_t VX1 = (V1xs*(Division-x_fract) + V1xe*(x_fract)) / Division;
	uint8_t VX2 = (V2xs*(Division-x_fract) + V2xe*(x_fract)) / Division;
	uint8_t VX3 = (V3xs*(Division-x_fract) + V3xe*(x_fract)) / Division;

	uint8_t VXY0 = (VX0*(Division-y_fract) + VX1*(y_fract)) / Division;
	uint8_t VXY1 = (VX2*(Division-y_fract) + VX3*(y_fract)) / Division;

	uint8_t VXYZ = (VXY0*(Division-z_fract) + VXY1*(z_fract)) / Division;

	return VXYZ;
}

uint8_t NoiseTable::Sample3d(uint32_t x, uint32_t y, uint32_t z)
{
	//uint8_t Octaves = 3;
	uint8_t Layer1Div = 8;
	uint8_t Layer2Div = 4;
	uint8_t Layer3Div = 2;
	
	uint8_t Layer1 = m_NoiseTable[z*m_Size*m_Size + y*m_Size + x];
	uint8_t Layer2 = TrilinearSample(x,y,z, 2);
	uint8_t Layer3 = TrilinearSample(x,y,z, 4);

	uint32_t Result32 = Layer1/Layer1Div + Layer2/Layer2Div + Layer3/Layer3Div;
	uint8_t Result;
	if(Result32 > 255)
		Result = 255;
	else
		Result = Result32;
	return Result;
}

inline float CLAMP(float V, float Min, float Max)
{
	float Result = V;
	if(Result < Min)
		Result = Min;
	else if(Result > Max)
		Result = Max;
	return Result;
}

float SignedDistanceFunctionTorus(int x, int y, int z, int Size)
{
	float X = (float)(x*2)/Size -1;
	float Y = (float)(y*2)/Size -1;
	float Z = (float)(z*2)/Size -1;

	X*=1.5f;
	Y*=1.5f;
	Z*=1.5f;

	Vec2f t = {0.8f, 0.2f};
	Vec2f q = Vec2f{LEN(Vec2f{X,Z}) -t.x, Y};
	float torus = LEN(q)-t.y;
	return CLAMP(torus, -1, 1);
}

float SignedDistanceFunctionSphere(Vec3f Center, float Radius, int x, int y, int z)
{
	Vec3f P = {x,y,z};
	float Dist = LEN(P - Center) - Radius;
	return CLAMP(Dist, -1, 1);
}

float SignedDistanceFunctionCylinder(Vec2V3 Line, float Radius, int x, int y, int z)
{
	Vec3f P = {x,y,z};
	Vec3f LineVec = Line.V2 - Line.V1;
	Vec3f SP = P - Line.V1;

	float FractionOfLine = CLAMP(DOT(SP, LineVec)/DOT(LineVec, LineVec), 0, 1);
	float Dist = LEN(SP - FractionOfLine*LineVec) - Radius;
	return CLAMP(Dist, -1, 1);
}

float SignedDistanceFunctionValley(Vec2V3 Line, float Radius, int x, int y, int z)
{
	Vec3f P = {x,y,z};
	Vec3f LineVec = Line.V2 - Line.V1;
	Vec3f SP = P - Line.V1;

	float FractionOfLine = CLAMP(DOT(SP, LineVec)/DOT(LineVec, LineVec), 0, 1);
	Vec3f PerpendicularVec = SP - FractionOfLine*LineVec;
	float Dist;
	float YDist = DOT(PerpendicularVec, {0,1,0});
	if(YDist < 0)	//below line
	{
		Dist = LEN(PerpendicularVec) - Radius;
	}
	else
	{
		Dist = LEN(PerpendicularVec + YDist*Vec3f{0,-1,0}) - Radius;
	}
	
	return CLAMP(Dist, -1, 1);
}


Map::Map(int Size)	//size if number of cubes
{
	m_Size=Size+1;
	m_Octree = Octree(m_Size);
	m_Map.resize((size_t)m_Size*m_Size*m_Size, 255);
	auto t0h = std::chrono::high_resolution_clock::now();
#if 0
	LSystem LS("Z");
	//LS.AddRule('Z', "FZ[RFZ]FZ[LFZ[RF]]");
	LS.AddRule('Z', "IZ[TIZ]IZ[TIZ[TI]]");
	std::cout << 0 << ": " << LS.State_ << "\n";
	for(int i = 1; i < 5; ++i)
	{
		LS.Iterate();
		std::cout << i << ": " << LS.State_ << "\n";
	}

	Vec3f Start = {0, m_Size-5, m_Size/2};
	std::vector<Vec2V3> PathLines = LS.DrawSystem(Start, {1,0,0});
	for(auto &L : PathLines)
	{
		int minx = std::min((int)std::max(std::min(L.V1.x, L.V2.x) -3, 0.0f), m_Size);
		int miny = std::min((int)std::max(std::min(L.V1.y, L.V2.y) -3, 0.0f), m_Size);
		int minz = std::min((int)std::max(std::min(L.V1.z, L.V2.z) -3, 0.0f), m_Size);

		int maxx = std::min((int)std::max(std::max(L.V1.x, L.V2.x) +3, 0.0f), m_Size);
		int maxy = m_Size;
		int maxz = std::min((int)std::max(std::max(L.V1.z, L.V2.z) +3, 0.0f), m_Size);
		for(int z = minz; z < maxz; ++z)
		{
			for(int y = miny; y < maxy; ++y)
			{
				for(int x = minx; x < maxx; ++x)
				{
					float Radius = 2.0f + (rand()%101)/100.0f;
					float Distance = SignedDistanceFunctionValley(L, Radius, x,y,z);
					uint8_t Value = 127+127*Distance;
		
					uint8_t CV = Get(x,y,z);
					if(Value < CV)
						Set(x,y,z, Value);
				}
			}
		}
	}
#else
	//for(int z = 0; z < m_Size; ++z)
	//{
	//	for(int y = 0; y < m_Size; ++y)
	//	{
	//		for(int x = 0; x < m_Size; ++x)
	//		{
	//			uint8_t Value = m_NoiseTable.Sample3d(x,y,z);
	//			Set(x,y,z, Value);
	//		}
	//	}
	//}
	for(int z = 0; z < m_Size; ++z)
	{
		for(int y = 0; y < m_Size; ++y)
		{
			for(int x = 0; x < m_Size; ++x)
			{
				uint8_t Value = y < m_Size/2 ? 255 : 0;
				Set(x,y,z, Value);
			}
		}
	}
#endif
	auto now = std::chrono::high_resolution_clock::now();
	float dt = std::chrono::duration<float>(now - t0h).count();
	std::cout << "Lines: " << dt*1000 << "ms\n";
}

void Map::Set(int x, int y, int z, uint8_t Val)
{
	m_Map[(size_t)z*m_Size*m_Size + (size_t)y*m_Size + x] = Val;
}

uint8_t Map::Get(int x, int y, int z)
{
	return m_Map[(size_t)z*m_Size*m_Size + (size_t)y*m_Size + x];
}

void AddTriangle(Vec3f Corners[3], Vec3f Colors[3],
				 std::vector<uint32_t> &Indices, std::vector<Vec3f> &VertexPos,
				 std::vector<Vec3f> &VertexCol, std::vector<Vec3f> &VertexNorm)
{
	uint32_t FirstIndex = VertexPos.size();
	VertexPos.push_back(Corners[0]);
	VertexPos.push_back(Corners[1]);
	VertexPos.push_back(Corners[2]);
	VertexCol.push_back(Colors[0]);
	VertexCol.push_back(Colors[1]);
	VertexCol.push_back(Colors[2]);
	Vec3f Normal = NORMALIZE(CROSS(Corners[1]-Corners[0], Corners[2]-Corners[1]));
	VertexNorm.push_back(Normal);
	VertexNorm.push_back(Normal);
	VertexNorm.push_back(Normal);

	Indices.push_back(FirstIndex+0);Indices.push_back(FirstIndex+1);Indices.push_back(FirstIndex+2);
}

void AddQuad(Vec3f Corners[4], Vec3f Colors[4],
				 std::vector<uint32_t> &Indices, std::vector<Vec3f> &VertexPos, std::vector<Vec3f> &VertexCol)
{
	uint32_t FirstIndex = VertexPos.size();
	VertexPos.push_back(Corners[0]);
	VertexPos.push_back(Corners[1]);
	VertexPos.push_back(Corners[2]);
	VertexPos.push_back(Corners[3]);
	VertexCol.push_back(Colors[0]);
	VertexCol.push_back(Colors[1]);
	VertexCol.push_back(Colors[2]);
	VertexCol.push_back(Colors[3]);

	Indices.push_back(FirstIndex+0);Indices.push_back(FirstIndex+1);Indices.push_back(FirstIndex+2);
	Indices.push_back(FirstIndex+2);Indices.push_back(FirstIndex+3);Indices.push_back(FirstIndex+0);
}

void AddCube(Vec3f Corners[8], Vec3f Colors[8],
				 std::vector<uint32_t> &Indices, std::vector<Vec3f> &VertexPos, std::vector<Vec3f> &VertexCol)
{
	uint32_t FirstIndex = VertexPos.size();
	VertexPos.push_back(Corners[0]);
	VertexPos.push_back(Corners[1]);
	VertexPos.push_back(Corners[2]);
	VertexPos.push_back(Corners[3]);
	VertexPos.push_back(Corners[4]);
	VertexPos.push_back(Corners[5]);
	VertexPos.push_back(Corners[6]);
	VertexPos.push_back(Corners[7]);
	VertexCol.push_back(Colors[0]);
	VertexCol.push_back(Colors[1]);
	VertexCol.push_back(Colors[2]);
	VertexCol.push_back(Colors[3]);
	VertexCol.push_back(Colors[4]);
	VertexCol.push_back(Colors[5]);
	VertexCol.push_back(Colors[6]);
	VertexCol.push_back(Colors[7]);

	Indices.push_back(FirstIndex+2);Indices.push_back(FirstIndex+6);Indices.push_back(FirstIndex+7);	//front
	Indices.push_back(FirstIndex+7);Indices.push_back(FirstIndex+3);Indices.push_back(FirstIndex+2);
	
	Indices.push_back(FirstIndex+0);Indices.push_back(FirstIndex+4);Indices.push_back(FirstIndex+5);	//back
	Indices.push_back(FirstIndex+5);Indices.push_back(FirstIndex+1);Indices.push_back(FirstIndex+0);

	Indices.push_back(FirstIndex+1);Indices.push_back(FirstIndex+5);Indices.push_back(FirstIndex+6);	//right
	Indices.push_back(FirstIndex+6);Indices.push_back(FirstIndex+2);Indices.push_back(FirstIndex+1);

	Indices.push_back(FirstIndex+0);Indices.push_back(FirstIndex+3);Indices.push_back(FirstIndex+7);	//left
	Indices.push_back(FirstIndex+7);Indices.push_back(FirstIndex+4);Indices.push_back(FirstIndex+0);

	Indices.push_back(FirstIndex+4);Indices.push_back(FirstIndex+7);Indices.push_back(FirstIndex+6);	//top
	Indices.push_back(FirstIndex+6);Indices.push_back(FirstIndex+5);Indices.push_back(FirstIndex+4);

	Indices.push_back(FirstIndex+0);Indices.push_back(FirstIndex+1);Indices.push_back(FirstIndex+2);	//bottom
	Indices.push_back(FirstIndex+2);Indices.push_back(FirstIndex+3);Indices.push_back(FirstIndex+0);
}

inline Vec3f CreateSurfacePoint(uint8_t i1, uint8_t i2, Vec3i CubeCorners[8], uint8_t Threshold, uint8_t CornerValues[8])
{
	Vec3f Vf1 = {CubeCorners[i1].x, CubeCorners[i1].y, CubeCorners[i1].z};
	Vec3f Vf2 = {CubeCorners[i2].x, CubeCorners[i2].y, CubeCorners[i2].z};
	uint8_t V1 = CornerValues[i1];
	uint8_t V2 = CornerValues[i2];
	float t = 0;
	if(V2-V1 != 0)
	{
		t = (float)(Threshold - V1) / (V2-V1);
		if(t == 0)
			t = 0.001f;
		if(t == 1)
			t = 0.999f;
	}
	Vec3f Result = Vf1+t*(Vf2-Vf1);

	return Result;
}

CubeTable CT;

void Map::MarchCube(std::vector<uint32_t> &Indices, std::vector<Vec3f> &VertexPos, std::vector<Vec3f> &VertexCol,
					std::vector<Vec3f> &VertexNorm, Vec3i CubeCorners[8], uint8_t Threshold)
{
	Vec3f NewPoints[12];
	bool OverThreshold[8] = {false};
	uint8_t NumOverThreshold = 0;
	uint8_t CornerValues[8];

	for(int i = 0; i<8; ++i)
	{
		uint8_t Val = Get(CubeCorners[i].x, CubeCorners[i].y, CubeCorners[i].z);
		CornerValues[i] = Val;
		OverThreshold[i] = (Val >= Threshold);
		NumOverThreshold += (Val >= Threshold);
	}
	if(NumOverThreshold == 0 || NumOverThreshold == 8)
		return;
	NewPoints[0] = CreateSurfacePoint(0, 1, CubeCorners, Threshold, CornerValues);	//create middle points
	NewPoints[1] = CreateSurfacePoint(1, 2, CubeCorners, Threshold, CornerValues);
	NewPoints[2] = CreateSurfacePoint(2, 3, CubeCorners, Threshold, CornerValues);
	NewPoints[3] = CreateSurfacePoint(3, 0, CubeCorners, Threshold, CornerValues);

	NewPoints[4] = CreateSurfacePoint(4, 5, CubeCorners, Threshold, CornerValues);
	NewPoints[5] = CreateSurfacePoint(5, 6, CubeCorners, Threshold, CornerValues);
	NewPoints[6] = CreateSurfacePoint(6, 7, CubeCorners, Threshold, CornerValues);
	NewPoints[7] = CreateSurfacePoint(7, 4, CubeCorners, Threshold, CornerValues);

	NewPoints[8] = CreateSurfacePoint(0, 4, CubeCorners, Threshold, CornerValues);
	NewPoints[9] = CreateSurfacePoint(1, 5, CubeCorners, Threshold, CornerValues);
	NewPoints[10] = CreateSurfacePoint(2, 6, CubeCorners, Threshold, CornerValues);
	NewPoints[11] = CreateSurfacePoint(3, 7, CubeCorners, Threshold, CornerValues);

	std::vector<uint8_t> Trigs = CT.GetCube(OverThreshold);
	for(uint8_t i = 0; i < Trigs.size(); i+=3)
	{
		Vec3f Corners[3] = 
		{
			NewPoints[Trigs[i]],
			NewPoints[Trigs[i+1]],
			NewPoints[Trigs[i+2]]
		};
		Vec3f Colors[3] = 
		{
			{1, 0.5f, 0.31f},
			{1, 0.5f, 0.31f},
			{1, 0.5f, 0.31f}
		};
		uint32_t FirstIndex = VertexPos.size();;
		AddTriangle(Corners, Colors, Indices, VertexPos, VertexCol, VertexNorm);
		uint32_t TrigIndex[3] = {FirstIndex, FirstIndex+1, FirstIndex+2};
		m_Octree.AddTriangle(TrigIndex, CubeCorners[0]);
		if(DOT(VertexNorm[FirstIndex], {0,1,0}) > 0.5f)
		{
			m_NavMesh.AddTriangle(Corners);
		}
	}
}

void Map::GetWorldVertexData(std::vector<uint32_t> &Indices, std::vector<Vec3f> &VertexPos, std::vector<Vec3f> &VertexCol,
							 std::vector<Vec3f> &VertexNorm, uint8_t Threshold)
{
	uint8_t Size = m_Size;
	VertexPos.reserve((uint64_t)Size*Size*Size);
	VertexCol.reserve((uint64_t)Size*Size*Size);
	Indices.reserve((uint64_t)Size*Size*Size*3);

	for(uint8_t z = 0; z < Size-1; ++z)
	{
		for(uint8_t y = 0; y < Size-1; ++y)
		{
			for(uint8_t x = 0; x < Size-1; ++x)
			{
				Vec3i Corners[8] =
				{
					{x,y,z}, {x+1,y,z},
					{x+1,y,z+1}, {x,y,z+1},
					{x,y+1,z}, {x+1,y+1,z},
					{x+1,y+1,z+1}, {x,y+1,z+1}
				};
				MarchCube(Indices, VertexPos, VertexCol, VertexNorm, Corners, Threshold);
			}
		}
	}
	m_Indices = Indices;
	m_VertexPos = VertexPos;
}

void Map::GetCorners(std::vector<uint32_t> &Indices, std::vector<Vec3f> &VertexPos, std::vector<Vec3f> &VertexCol)
{
	uint8_t Size = m_Size;
	VertexPos.reserve((uint64_t)Size*Size*Size);
	VertexCol.reserve((uint64_t)Size*Size*Size);
	Indices.reserve((uint64_t)Size*Size*Size*3);
	for(uint8_t z = 0; z < Size-1; ++z)
	{
		for(uint8_t y = 0; y < Size-1; ++y)
		{
			for(uint8_t x = 0; x < Size-1; ++x)
			{
				Vec3f Corners[8] =
				{
					{x,y,z}, {x+1,y,z},
					{x+1,y,z+1}, {x,y,z+1},
					{x,y+1,z}, {x+1,y+1,z},
					{x+1,y+1,z+1}, {x,y+1,z+1}
				};
				Vec3f Colors[8] =
				{
					{Get(x,y,z)},
					{Get(x+1,y,z)},
					{Get(x+1,y,z+1)},
					{Get(x,y,z+1)},
					{Get(x,y+1,z)},
					{Get(x+1,y+1,z)},
					{Get(x+1,y+1,z+1)},
					{Get(x,y+1,z+1)}
				};
				AddCube(Corners, Colors, Indices, VertexPos, VertexCol);
			}
		}
	}
}

bool Map::RayWorldInterSect(Vec3f Origin, Vec3f Direction, float &tmax)
{
	bool Result = false;
	//for(size_t i = 0; i < Indices_.size(); i+=3)
	//{
	//	Vec3f Triangle[3] = {VertexPos_[Indices_[i]], VertexPos_[Indices_[i+1]], VertexPos_[Indices_[i+2]]};
	//	if(RayTriangleIntersection(Origin, Direction, Triangle, tmax))
	//	{
	//		Result = true;
	//	}
	//}
	std::vector<uint32_t> Possible = m_Octree.Intersect(Origin, Direction, tmax);
	for(size_t i = 0; i < Possible.size(); i+=3)
	{
		Vec3f Triangle[3] = {m_VertexPos[Possible[i]], m_VertexPos[Possible[i+1]], m_VertexPos[Possible[i+2]]};
		if(RayTriangleIntersection(Origin, Direction, Triangle, tmax))
		{
			Result = true;
		}
	}
	return Result;
}
