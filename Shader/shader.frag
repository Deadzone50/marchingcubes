#version 330 core
in vec3 VertexColor;
in vec3 VertexNormal;
in vec3 FragPos;

out vec4 FragColor;

struct LightStruct
{
	vec3 Pos;
	vec3 Color;
};

//struct MaterialStruct
//{
//	vec3 Ambient;
//	vec3 Diffuse;
//	vec3 Specular;
//	float Shininess;
//};

//uniform MaterialStruct Material;
uniform vec3 CameraPos;

uniform int NumLights;
uniform float LightFarPlane;
uniform LightStruct Lights[8];
uniform samplerCube DepthMaps[8];

vec3 SampleOffsetDir[] = vec3[]
(
   vec3(1,1,1), vec3(-1,-1,-1), vec3(-1,1,-1), vec3(1,-1, 1),
   vec3(1,1,-1), vec3(-1,-1,1), vec3(1,-1,-1), vec3(-1,1,1),
   vec3(1,1,0), vec3(1,-1,0), vec3(-1,-1,0), vec3(-1,1,0),
   vec3(1,0,1), vec3(-1,0,1), vec3(1,0,-1), vec3(-1,0,1),
   vec3(0,1,1), vec3(0,-1,1), vec3(0,-1,-1), vec3(0,1,-1)
);

float ShadowCalc(vec3 FragPos, vec3 LightPos, samplerCube DepthMap)
{
	vec3 LightToFrag = FragPos - LightPos;
	float CurrentDepth = length(LightToFrag);
	float Shadow = 0.0;
	float Bias = 0.1;
	const int samples = 8;
//	float offset = 0.1;
//	float offstep = offset/4;
//	float radius = 0.05;
	for(int i = 0; i < samples; ++i)
	{
//		float x = -offset + (i/16)*offstep;
//		float y = -offset + (i/4)*offstep;
//		float z = -offset + (i%4)*offstep;
//		vec3 Off = vec3(x,y,z);

		vec3 Off = 0.05 * SampleOffsetDir[i];

		float ClosestDepth = texture(DepthMap, LightToFrag + Off).r;
		ClosestDepth  *= LightFarPlane;
		Shadow += CurrentDepth - Bias > ClosestDepth ? 1.0 : 0.0;
	}
	Shadow /=float(samples);
	return Shadow;
}

vec3 CalcPointLight(LightStruct Light, vec3 Normal, vec3 FragPos, vec3 ViewDir, samplerCube DepthMap)
{
	float LightDistance = length(Light.Pos-FragPos);
	float LightAttenuation = 1.0 / (1 + 0.09*LightDistance + 0.032*LightDistance*LightDistance);
	//ambient
	vec3 Ambient = 0.1*Light.Color;
	//diffuse
	vec3 LightDir = normalize(Light.Pos - FragPos);
	float Diff = max(dot(Normal, LightDir), 0.0);
	vec3 Diffuse = Diff*Light.Color;
	//specular
	float SpecularStrenght = 0.5;
	vec3 ReflectDir = reflect(-LightDir, Normal);
	float Spec = pow(max(dot(ViewDir, ReflectDir), 0.0), 32);
	vec3 Specular = SpecularStrenght * Spec * Light.Color;
	//shadow
	float Shadow = ShadowCalc(FragPos, Light.Pos, DepthMap);
	return (Ambient+ (1.0 - Shadow)*(Diffuse+Specular))*LightAttenuation;
}

void main()
{
	vec3 Color = VertexColor;
	vec3 Normal = normalize(VertexNormal);
	vec3 ViewDir = normalize(CameraPos - FragPos);

	vec3 TotalLight;
	if(NumLights > 0)
	{
		for(int i = 0; i < NumLights; ++i)
		{
			TotalLight += CalcPointLight(Lights[i], Normal, FragPos, ViewDir, DepthMaps[i]);
		}
	}
	else
	{
		TotalLight = vec3(1,1,1);
	}

	vec3 Result = Color*TotalLight;
	if(gl_FrontFacing)
	{
		FragColor = vec4(Result, 1.0);
	}
	else
	{
		FragColor = vec4(0.0, 0.0, 0.0, 1.0);
	}
}
