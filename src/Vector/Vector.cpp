#include "Vector.h"

#include <cmath>
#include <string>
#include <sstream>

Vec2f MINMAX(float f1, float f2)
{
	if(f1 < f2)
		return Vec2f{f1,f2};
	else
		return Vec2f{f2,f1};
}

Vec3f NORMALIZE(Vec3f V)
{
	float OneOverLen = 1/sqrtf(V.x*V.x + V.y*V.y + V.z*V.z);
	Vec3f VN ={OneOverLen*V.x, OneOverLen*V.y, OneOverLen*V.z};
	return VN;
}
Vec2f NORMALIZE(Vec2f V)
{
	float OneOverLen = 1/sqrt(V.x*V.x + V.y*V.y);
	Vec2f VN ={OneOverLen*V.x, OneOverLen*V.y};
	return VN;
}

float LEN(Vec3f V)
{
	return sqrt(V.x*V.x + V.y*V.y + V.z*V.z);
}
float LEN2(Vec3f V)
{
	return V.x*V.x + V.y*V.y + V.z*V.z;
}

float LEN(Vec2f V)
{
	return sqrt(V.x*V.x + V.y*V.y);
}

float SUM(Vec3f V)
{
	return V.x+V.y+V.z;
}

Vec3f CROSS(Vec3f A, Vec3f B)
{
	Vec3f C{A.y*B.z-A.z*B.y, A.z*B.x-A.x*B.z, A.x*B.y-A.y*B.x};
	return C;
}
float DOT(Vec4f V1, Vec4f V2)
{
	float f = V1.x*V2.x + V1.y*V2.y + V1.z*V2.z + V1.w*V2.w;
	return f;
}
float DOT(Vec3f V1, Vec3f V2)
{
	float f = V1.x*V2.x + V1.y*V2.y + V1.z*V2.z;
	return f;
}
float DOT(Vec2f V1, Vec2f V2)
{
	float f = V1.x*V2.x + V1.y*V2.y;
	return f;
}

Vec3f ABS(Vec3f V)
{
	Vec3f v;
	v.x = abs(V.x);
	v.y = abs(V.y);
	v.z = abs(V.z);
	return v;
}