#include "Drawing.h"

Drawable::Drawable(/*Shader Shader,*/ GLenum DrawType, bool Colors, bool Normals)
{
	//Shader_ = Shader;
	DrawType_ = DrawType;
	glGenVertexArrays(1, &VAO_);
	glGenBuffers(1, &VBO_Pos_);
	glGenBuffers(1, &EBO_);
	if(Colors)
	{
		glGenBuffers(1, &VBO_Col_);
	}
	if(Normals)
	{
		glGenBuffers(1, &VBO_Norm_);
	}
}

void Drawable::SetBuffers(std::vector<uint32_t> Indices, std::vector<Vec3f> VertexPos, std::vector<Vec3f> VertexCol,
					  std::vector<Vec3f> VertexNorm)
{
	NumElements_ = Indices.size();
	glBindVertexArray(VAO_);
	// position
	glBindBuffer(GL_ARRAY_BUFFER, VBO_Pos_);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vec3f)*VertexPos.size(), VertexPos.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vec3f), (void*)0);
	glEnableVertexAttribArray(0);
	// color
	if(!VertexCol.empty())
	{
		glBindBuffer(GL_ARRAY_BUFFER, VBO_Col_);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vec3f)*VertexCol.size(), VertexCol.data(), GL_STATIC_DRAW);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vec3f), (void*)0);
		glEnableVertexAttribArray(1);
	}
	// normal
	if(!VertexNorm.empty())
	{
		glBindBuffer(GL_ARRAY_BUFFER, VBO_Norm_);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vec3f)*VertexNorm.size(), VertexNorm.data(), GL_STATIC_DRAW);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vec3f), (void*)0);
		glEnableVertexAttribArray(2);
	}
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO_);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t)*Indices.size(), Indices.data(), GL_STATIC_DRAW);
}

//void Drawable::UseShader(Mat4f ModelMatrix, Mat4f ViewMatrix, Mat4f ProjectionMatrix)
//{
//	Shader_.Use();
//	Shader_.SetMat4("Projection", ProjectionMatrix);
//	Shader_.SetMat4("View", ViewMatrix);
//	Shader_.SetMat4("Model", ModelMatrix);
//}
//
//Shader &Drawable::GetShader()
//{
//	return Shader_;
//}

void Drawable::Draw()
{
	glBindVertexArray(VAO_);
	glDrawElements(DrawType_, NumElements_, GL_UNSIGNED_INT, (void*)0);
}

void DebugDrawing::Init()
{
	PointsDrawable_ = Drawable(GL_POINTS, true, false);
	LinesDrawable_ = Drawable(GL_LINES, true, false);
	TrianglesDrawable_ = Drawable(GL_TRIANGLES, true, false);
}

void DebugDrawing::DrawPoint(Vec3f Pos, Vec3f Col)
{
	PointPos_.push_back(Pos);
	PointCol_.push_back(Col);
	PointIndex_.push_back(PointIndex_.size());
}

void DebugDrawing::DrawLine(Vec3f Start, Vec3f End, Vec3f Col)
{
	LinePos_.push_back(Start);
	LinePos_.push_back(End);
	LineCol_.push_back(Col);
	LineCol_.push_back(Col);
	LineIndex_.push_back(LineIndex_.size());
	LineIndex_.push_back(LineIndex_.size());
}

void DebugDrawing::DrawTriangle(Vec3f Corners[3], Vec3f Col)
{
	TrianglePos_.push_back(Corners[0]);
	TrianglePos_.push_back(Corners[1]);
	TrianglePos_.push_back(Corners[2]);
	TriangleCol_.push_back(Col);
	TriangleCol_.push_back(Col);
	TriangleCol_.push_back(Col);
	TriangleIndex_.push_back(TriangleIndex_.size());
	TriangleIndex_.push_back(TriangleIndex_.size());
	TriangleIndex_.push_back(TriangleIndex_.size());
}

void DebugDrawing::DrawPolygon(std::vector<Vec3f> Corners, Vec3f Col)
{
	Vec3f Center = {0,0,0};
	uint32_t Num = 0;
	for(auto &C : Corners)
	{
		Center += C;
		++Num;
	}
	Center = Center/Num;
	for(uint32_t i = 0; i < Corners.size()-1; ++i)
	{
		Vec3f T[3] = {Corners[i] + Vec3f{0,0.01f,0}, Corners[i+1] + Vec3f{0,0.01f,0}, Center + Vec3f{0,0.01f,0}};
		DrawTriangle(T, Col);
	}
}

void DebugDrawing::UpdateBuffers()
{
	PointsDrawable_.SetBuffers(PointIndex_, PointPos_, PointCol_);
	LinesDrawable_.SetBuffers(LineIndex_, LinePos_, LineCol_);
	TrianglesDrawable_.SetBuffers(TriangleIndex_, TrianglePos_, TriangleCol_);
}

void DebugDrawing::Clear()
{
	PointPos_.clear();
	PointCol_.clear();
	PointIndex_.clear();
	LinePos_.clear();
	LineCol_.clear();
	LineIndex_.clear();
	TrianglePos_.clear();
	TriangleCol_.clear();
	TriangleIndex_.clear();
	UpdateBuffers();
}

void DebugDrawing::Draw()
{
	PointsDrawable_.Draw();
	LinesDrawable_.Draw();
	TrianglesDrawable_.Draw();
}
