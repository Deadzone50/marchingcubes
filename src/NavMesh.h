#pragma once

#include "Vector/Vector.h"
#include <vector>

struct NavPoint;

struct Edge
{
	NavPoint* N1;
	NavPoint* N2;
	bool Marked = false;
};

struct NavPoint
{
	Vec3f Pos;
	std::vector<Edge> Edges;
};

class NavZone
{
public:
	bool IsInside(Vec3f Point);
	//bool AppendIfBordering(Vec3f Trig[3]);
	std::vector<NavZone*> GetNeighbours();

	std::vector<Vec3f> m_Corners;
	std::vector<NavZone*> m_Neighbours;
};

class NavMesh
{
public:
	void AddTriangle(Vec3f Corners[3]);
	void Generate();

	std::vector<Vec3f> GetRoute(Vec3f Start, Vec3f End);

	std::vector<NavZone> m_NavZones;
	std::vector<NavPoint*> m_Points;
};
