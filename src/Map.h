#pragma once

#include "Vector/Vector.h"
#include "Collision.h"
#include "NavMesh.h"

class NoiseTable
{
public:
	NoiseTable(uint32_t Size);
	uint8_t TrilinearSample(uint32_t x, uint32_t y, uint32_t z, uint8_t Division);
	//void SetLayers(uint8_t Num);
	//uint8_t Sample2d(uint32_t x, uint32_t y);
	uint8_t Sample3d(uint32_t x, uint32_t y, uint32_t z);

	uint32_t m_Size;
	std::vector<uint8_t> m_NoiseTable;
};

class PlayerClass;
class NPC;

class Map
{
public:
	Map(int Size);
	void Set(int x, int y, int z, uint8_t Val);
	uint8_t Get(int x, int y, int z);
	void GetWorldVertexData(std::vector<uint32_t> &Indices, std::vector<Vec3f> &VertexPos, std::vector<Vec3f> &VertexCol,
							std::vector<Vec3f> &VertexNorm, uint8_t Threshold);
	void GetCorners(std::vector<uint32_t> &Indices, std::vector<Vec3f> &VertexPos, std::vector<Vec3f> &VertexCol);
	void MarchCube(std::vector<uint32_t> &Indices, std::vector<Vec3f> &VertexPos, std::vector<Vec3f> &VertexCol,
				   std::vector<Vec3f> &VertexNorm, Vec3i CubeCorners[8], uint8_t Threshold);

	bool RayWorldInterSect(Vec3f Origin, Vec3f Direction, float &tmax);
	std::vector<Vec3f> GetRoute(Vec3f Start, Vec3f End) {return m_NavMesh.GetRoute(Start, End);}

	void AddPlayer(PlayerClass *Player) {Player_ = Player;}
	void AddEnemy(NPC *Enemy) {Enemy_ = Enemy;}
	PlayerClass* GetPlayer() {return Player_;}
	NPC* GetEnemy() {return Enemy_;}

	int m_Size;	//number of points
	std::vector<uint8_t> m_Map;
	NoiseTable m_NoiseTable = NoiseTable(128);

	std::vector<uint32_t> m_Indices;
	std::vector<Vec3f> m_VertexPos;
	Octree m_Octree;
	NavMesh m_NavMesh;
	PlayerClass *Player_;
	NPC *Enemy_;
};
