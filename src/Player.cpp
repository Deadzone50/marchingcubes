#include "Player.h"
#include "Collision.h"

void PlayerClass::SetRotation(Vec2f Angles)
{
	Rotation_ = Angles;

	LookingDirection_.x = cos(Rotation_.x) * cos(Rotation_.y);
	LookingDirection_.y = sin(Rotation_.x);
	LookingDirection_.z = cos(Rotation_.x) * sin(Rotation_.y);
	LookingDirection_ = NORMALIZE(LookingDirection_);

	Right_ = NORMALIZE(CROSS(LookingDirection_, { 0,1,0 }));
	Up_ = NORMALIZE(CROSS(Right_, LookingDirection_));
}

void PlayerClass::Rotate(Vec2f Angles)
{
	Rotation_ += Angles;

	if(Rotation_.x > 1.55f)
		Rotation_.x = 1.55f;
	if(Rotation_.x < -1.55f)
		Rotation_.x = -1.55f;

	SetRotation(Rotation_);
}

Mat4f PlayerClass::GetViewMatrix()
{
	return LookAt(Pos_, Pos_+LookingDirection_, Up_);
}

void PlayerClass::Jump()
{
	if(!Falling_)
	{
		Vec3f CurrentPos = GetPosition();
		CurrentSpeed_.y = 300.0f;
		SetPosition(CurrentPos + Vec3f{0,0.3f,0});
		Falling_ = true;
	}
}

void PlayerClass::CheckCollisions(Map &World)
{
	Vec3f CurrentPos = GetPosition();

	float t = 2.0f;
	if(World.RayWorldInterSect(CurrentPos, {0,-1,0}, t))	//floor, TODO: fix out of bounds
	{
		if(t < 1.8f && t > 0.2f)
		{
			CurrentPos += (1.81f-t)*Vec3f{0,1,0};
		}
		Falling_ = false;
	}
	else
	{
		Falling_ = true;
	}
	t = 0.2f;
	if(World.RayWorldInterSect(CurrentPos, {0,1,0}, t))		//ceiling
	{
		CurrentPos += (0.21f-t)*Vec3f{0,-1,0};
		CurrentSpeed_.y = 0;
	}

	const float Radius = 0.5f;
	const Vec3f Dir[8] = {
		{1,0,0}, {-1,0,0}, {0,0,1}, {0,0,-1},
		{0.7f,0,0.7f}, {-0.7f,0,0.7f}, {-0.7f,0,-0.7f}, {0.7f,0,-0.7f}
	};
	for(uint8_t i = 0; i < 8; ++i)							//sides
	{
		t = Radius;
		if(World.RayWorldInterSect(CurrentPos, Dir[i], t))
		{
			CurrentPos += (Radius-t)*(-Dir[i]);
		}
	}
	SetPosition(CurrentPos);
}

void PlayerClass::Step(float dt, Map &World)
{
	CheckCollisions(World);
	if(Falling_ && !Debug_)
	{
		CurrentSpeed_.y -= 9.81f;
	}
	else
	{
		CurrentSpeed_.y = 0;
	}
	Vec3f CurrentPos = GetPosition();
	SetPosition(CurrentPos + 0.1f*dt*CurrentSpeed_);
}
