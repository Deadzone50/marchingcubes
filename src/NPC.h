#pragma once
#include "Character.h"
#include "Map.h"

class NPC : public Character
{
public:
	void StepAI(float dt, Map &World);

	void CheckCollisions(Map &World);
	void Step(float dt, Map &World);
};
