#version 330 core
layout (location = 0) in vec3 VPos;

uniform mat4 Model;

void main()
{
	gl_Position = Model * vec4(VPos, 1.0);
}
