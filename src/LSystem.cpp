#include "Vector/Matrix.h"
#include "LSystem.h"

LSystemRule::LSystemRule(char In, std::string Out)
{
	In_ = In;
	Out_ = Out;
}

bool LSystemRule::Match(char c)
{
	return(c == In_);
}

std::string LSystemRule::Apply(char c)
{
	std::string Result;
	for(auto &c : Out_)
	{
		if(c == 'T')
		{
			uint8_t Dir = rand()%4;
			switch(Dir)
			{
				case 0:
					Result += 'L';
					break;
				case 1:
					Result += 'R';
					break;
				case 2:
					Result += 'L';
					break;
				case 3:
					Result += 'R';
					break;
				default:
					break;
			}
		}
		else if(c == 'I')
		{
			uint8_t Num = 1 + rand()%5;
			for(uint8_t i = 0; i < Num; ++i)
			{
				Result += 'F';
			}
		}
		else
		{
			Result += c;
		}
	}
	return Result;
}

LSystem::LSystem(std::string Start)
{
	State_ = Start;
}

void LSystem::AddRule(char In, std::string Out)
{
	Rules_.emplace_back(In, Out);
}

void LSystem::Iterate()
{
	std::string Result;
	for(auto c : State_)
	{
		bool Match = false;
		for(auto r : Rules_)
		{
			Match = r.Match(c);
			if(Match)
			{
				std::string res = r.Apply(c);
				Result += res;
				break;
			}
		}
		if(!Match)
		{
			Result += c;
		}
	}
	State_ = Result;
}

/*
	Basic:
	F : Forward
	R : Right	45*
	L : Left
	U : Up
	D : Down
	Z : Start
	0 : End
	[]: Start/End branch

	Blocks
	B : Split
	C : Curve
	H : Slope that returns to horizontal
	I : Straight line
	T : Turn

	Z -> FZ[RFZ]
	Z -> FZ[RFZ]FZ[LFZ[RF]]
*/

struct CurrentState
{
	Vec3f Dir, Up, R, Pos;
};

std::vector<Vec2V3> LSystem::DrawSystem(Vec3f Start, Vec3f Dir)
{
	std::vector<Vec2V3> Path;
	//Path.push_back(Start);
	CurrentState CS;
	CS.Pos = Start;
	CS.Dir = Dir;
	CS.Up = {0,1,0};
	CS.R = NORMALIZE(CROSS(CS.Dir, CS.Up));
	Mat3f R;
	std::vector<CurrentState> BranchStack;
	for(auto c : State_)
	{
		switch(c)
		{
			case 'F':
				Vec3f S = CS.Pos;
				Vec3f E = CS.Pos+CS.Dir;
				CS.Pos = E; 
				Path.push_back({S, E});
				break;
			case 'R':
				R.Identity();
				R.Rotate(CS.Up, -45*3.14f/180);
				CS.Dir = R*CS.Dir;
				CS.R = R*CS.R;
				break;
			case 'L':
				R.Identity();
				R.Rotate(CS.Up, 45*3.14f/180);
				CS.Dir = R*CS.Dir;
				CS.R = R*CS.R;
				break;
			case 'U':
				R.Identity();
				R.Rotate(CS.R, 45*3.14f/180);
				CS.Dir = R*CS.Dir;
				CS.Up = R*CS.Up;
				break;
			case 'D':
				R.Identity();
				R.Rotate(CS.R, -45*3.14f/180);
				CS.Dir = R*CS.Dir;
				CS.Up = R*CS.Up;
				break;
			case '[':
				BranchStack.push_back(CS);
				break;
			case ']':
				CS = BranchStack.back();
				BranchStack.pop_back();
				break;
			default:
				break;
		}
	}
	return Path;
}
