#version 330 core
in vec4 FragPos;

uniform vec3 LightPos;
uniform float FarPlane;

void main()
{
	float LightDistance = length(FragPos.xyz - LightPos);
	LightDistance = LightDistance / FarPlane;	//map to [0 1] range
	gl_FragDepth = LightDistance;
}
