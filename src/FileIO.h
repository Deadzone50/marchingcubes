#pragma once
#include <vector>
#include "Vector/Vector.h"

void ParseModelFile(std::string FileName, std::vector<uint32_t> &Indices, std::vector<Vec3f> &VertexPos);

void BoxMesh(float X, float Y, float Z, Vec3f Col, std::vector<uint32_t> &Indices, std::vector<Vec3f> &VertexPos,
				std::vector<Vec3f> &VertexCol, std::vector<Vec3f> &VertexNorm);