#pragma once
#include <vector>
#include "Vector/Vector.h"

struct OctNode
{
	Vec3i AABB[2] = {0};
	OctNode *Parent = NULL;
	OctNode *Children[8] = {NULL};
	std::vector<uint32_t> Triangles;
};

class Octree
{
public:
	Octree() {};
	Octree(uint8_t WorldSize);

	void AddTriangle(uint32_t TriangeIndex[3], Vec3i CubeCorner0);
	void AddCube(Vec3i CubeCorner, std::vector<uint32_t> &TriangleIndices, std::vector<Vec3f> &TrianglePos);

	std::vector<uint32_t> Intersect(Vec3f Origin, Vec3f Direction, float &tmax);

	OctNode RootNode_;
};

int RayTriangleIntersection(Vec3f Origin, Vec3f Direction, Vec3f Triangle[3], float &tmax);
bool RayAABBIntersection(Vec3f Origin, Vec3f Direction, Vec3i AABB[2], float &tmax);
