#include "FileIO.h"

void CreateQuad(Vec3f C0, Vec3f C1, Vec3f C2, Vec3f C3, Vec3f Col, Vec3f Normal, std::vector<uint32_t> &Indices, std::vector<Vec3f> &VertexPos,
				std::vector<Vec3f> &VertexCol, std::vector<Vec3f> &VertexNorm)
{
	uint32_t First = VertexPos.size();
	VertexPos.push_back(C0);
	VertexPos.push_back(C1);
	VertexPos.push_back(C2);
	VertexPos.push_back(C3);
	VertexNorm.push_back(Normal);
	VertexNorm.push_back(Normal);
	VertexNorm.push_back(Normal);
	VertexNorm.push_back(Normal);
	VertexCol.push_back(Col);
	VertexCol.push_back(Col);
	VertexCol.push_back(Col);
	VertexCol.push_back(Col);
	Indices.push_back(First + 0); Indices.push_back(First + 1); Indices.push_back(First + 2);
	Indices.push_back(First + 2); Indices.push_back(First + 3); Indices.push_back(First + 0);
}

void BoxMesh(float X, float Y, float Z, Vec3f Col, std::vector<uint32_t> &Indices, std::vector<Vec3f> &VertexPos,
			 std::vector<Vec3f> &VertexCol, std::vector<Vec3f> &VertexNorm)
{
	Vec3f C[] = 
	{
		{-X/2, -Y/2, -Z/2},
		{X/2, -Y/2, -Z/2},
		{X/2, -Y/2, Z/2},
		{-X/2, -Y/2, Z/2},

		{-X/2, Y/2, -Z/2},
		{X/2, Y/2, -Z/2},
		{X/2, Y/2, Z/2},
		{-X/2, Y/2, Z/2}
	};
	CreateQuad(C[0], C[1], C[2], C[3], Col, {0,-1,0}, Indices, VertexPos, VertexCol, VertexNorm);	//Bottom
	CreateQuad(C[7], C[6], C[5], C[4], Col, {0,1,0}, Indices, VertexPos, VertexCol, VertexNorm);	//Top

	CreateQuad(C[1], C[5], C[6], C[2], Col, {-1,0,0}, Indices, VertexPos, VertexCol, VertexNorm);	//Left
	CreateQuad(C[4], C[0], C[3], C[7], Col, {1,0,0}, Indices, VertexPos, VertexCol, VertexNorm);	//Right

	CreateQuad(C[2], C[6], C[7], C[3], Col, {0,0,1}, Indices, VertexPos, VertexCol, VertexNorm);	//Front
	CreateQuad(C[0], C[4], C[5], C[1], Col, {0,0,-1}, Indices, VertexPos, VertexCol, VertexNorm);	//Back
}
