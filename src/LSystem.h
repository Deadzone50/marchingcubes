#pragma once
#include <vector>
#include <string>
#include "Vector/Vector.h"

class LSystemRule
{
public:
	LSystemRule(char In, std::string Out);
	bool Match(char c);
	std::string Apply(char c);
	char In_;
	std::string Out_;
};

class LSystem
{
public:
	LSystem(std::string Start);
	void AddRule(char In, std::string Out);
	void Iterate();
	std::vector<Vec2V3> DrawSystem(Vec3f Start, Vec3f Dir);

	std::string State_;
	std::vector<LSystemRule> Rules_;
};
