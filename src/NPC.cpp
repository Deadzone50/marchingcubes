#include "NPC.h"
#include "Player.h"

void NPC::CheckCollisions(Map &World)
{
	Vec3f CurrentPos = GetPosition();

	float t = 2.0f;
	if(World.RayWorldInterSect(CurrentPos, {0,-1,0}, t))	//TODO: fix out of bounds
	{
		if(t < 1.8f && t > 0.2f)
		{
			CurrentPos += (1.81f-t)*Vec3f{0,1,0};
		}
		Falling_ = false;
	}
	else
	{
		Falling_ = true;
	}
	t = 0.2f;
	if(World.RayWorldInterSect(CurrentPos, {0,1,0}, t))
	{
		CurrentPos += (0.21f-t)*Vec3f{0,-1,0};
		CurrentSpeed_.y = 0;
	}

	const float Radius = 0.5f;
	const Vec3f Dir[8] = {
		{1,0,0}, {-1,0,0}, {0,0,1}, {0,0,-1},
		{0.7f,0,0.7f}, {-0.7f,0,0.7f}, {-0.7f,0,-0.7f}, {0.7f,0,-0.7f}
	};
	for(uint8_t i = 0; i < 8; ++i)
	{
		t = Radius;
		if(World.RayWorldInterSect(CurrentPos, Dir[i], t))
		{
			CurrentPos += (Radius-t)*(-Dir[i]);
		}
	}
	SetPosition(CurrentPos);
}

void NPC::StepAI(float dt, Map &World)
{
	PlayerClass *Player = World.GetPlayer();
	Vec3f Target = Player->GetPosition();
	Vec3f CurrentPos = GetPosition();

	Vec3f NewDir;
	Vec3f Forward = NORMALIZE(LookingDirection_);
	Vec3f ToPlayer = NORMALIZE(Target-CurrentPos);
	float CosAngle = DOT(Forward, ToPlayer);
	float MaxAngle =  5*dt;
	if(acos(CosAngle) > MaxAngle)
	{
		Vec3f UP = CROSS(Forward, ToPlayer);
		Mat3f R;
		R.Rotate(UP, MaxAngle);
		NewDir = R*Forward;
	}
	else
	{
		NewDir = ToPlayer;
	}
	SetDirection(NewDir);
	Move({0,0,-1.0f*dt});

	//SetPosition(CurrentPos + 0.1f*dt*Vec3f{10,0,0});
}

void NPC::Step(float dt, Map &World)
{
	StepAI(dt, World);

	//CheckCollisions(World);
	//if(Falling_ && !Debug_)
	//{
	//	CurrentSpeed_.y -= 9.81f;
	//}
	//else
	//{
	//	CurrentSpeed_.y = 0;
	//}
	//Vec3f CurrentPos = GetPosition();
	//SetPosition(CurrentPos + 0.1f*dt*CurrentSpeed_);
}
