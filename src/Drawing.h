#pragma once
#include <vector>
#include "Shader.h"

class Drawable
{
public:
	Drawable() {};
	Drawable(/*Shader Shader,*/ GLenum DrawType, bool Colors, bool Normals);

	void SetBuffers(std::vector<uint32_t> Indices, std::vector<Vec3f> VertexPos,
					std::vector<Vec3f> VertexCol = {},
					std::vector<Vec3f> VertexNorm = {});

	//void UseShader(Mat4f ModelMatrix, Mat4f ViewMatrix, Mat4f ProjectionMatrix);
	//Shader &GetShader();
	void Draw();

	uint32_t NumElements_ = 0;
	unsigned int VAO_ = 0, VBO_Pos_ = 0, VBO_Col_ = 0, VBO_Norm_ = 0, EBO_ = 0;
	//Shader Shader_;
	GLenum DrawType_ = 0;
};

class DebugDrawing
{
public:
	void Init();

	void DrawPoint(Vec3f Pos, Vec3f Col  = {1,1,1});
	void DrawLine(Vec3f Start, Vec3f End, Vec3f Col = {1,1,1});
	void DrawTriangle(Vec3f Corners[3], Vec3f Col = {1,1,1});
	void DrawPolygon(std::vector<Vec3f> Corners, Vec3f Col = {1,1,1});

	void UpdateBuffers();
	void Clear();
	void Draw();

	Drawable PointsDrawable_;
	Drawable LinesDrawable_;
	Drawable TrianglesDrawable_;

	std::vector<uint32_t> PointIndex_;
	std::vector<Vec3f> PointPos_;
	std::vector<Vec3f> PointCol_;
	std::vector<uint32_t> LineIndex_;
	std::vector<Vec3f> LinePos_;
	std::vector<Vec3f> LineCol_;
	std::vector<uint32_t> TriangleIndex_;
	std::vector<Vec3f> TrianglePos_;
	std::vector<Vec3f> TriangleCol_;
};
