#pragma once

#ifndef __GNUC__
#pragma warning( disable : 4201 ) 
#endif

#include <sstream>
//#define FLT_MAX 1e30

union Vec3f
{
	struct
	{
		float x;
		float y;
		float z;
	};
	float V[3];
	//__m128 sse;
};

union Vec4f
{
	struct
	{
		float x;
		float y;
		float z;
		float w;
	};
	//__m128 sse;
	Vec4f() {}
	Vec4f(Vec3f V, float f) {x = V.x; y = V.y; z = V.z; w = f;}
	Vec4f(float f1,float f2,float f3,float f4) {x = f1; y = f2; z = f3; w = f4;}
	Vec3f GetXYZ();
};

union Vec3i
{
	struct
	{
		int x;
		int y;
		int z;
	};
	struct
	{
		int P1;
		int P2;
		int P3;
	};
	int V[3];
};

struct Vec2f
{
	float x;
	float y;
};

struct Vec2V3
{
	Vec3f V1;
	Vec3f V2;
};

struct Vec2V2
{
	Vec2f V1;
	Vec2f V2;
};

struct Vec2i
{
	int x;
	int y;
};

Vec3f NORMALIZE(Vec3f V);
Vec2f NORMALIZE(Vec2f V);
Vec3f CROSS(Vec3f A, Vec3f B);
float DOT(Vec4f V1, Vec4f V2);
float DOT(Vec3f V1, Vec3f V2);
float DOT(Vec2f V1, Vec2f V2);
float SUM(Vec3f V);
float LEN2(Vec3f V);
float LEN(Vec3f V);
float LEN(Vec2f V);
Vec2f MINMAX(float f1, float f2);
Vec3f ABS(Vec3f V);

///Vec4f
bool  operator==(const Vec4f &V1, const Vec4f &V2);
Vec4f operator*(const Vec4f &V1, const Vec4f &V2);
Vec4f operator+(const Vec4f &V1, const Vec4f &V2);
Vec4f operator-(const Vec4f &V1, const Vec4f &V2);
void  operator*=(Vec4f &V1, const Vec4f &V2);
void  operator+=(Vec4f &V1, const Vec4f &V2);
void  operator-=(Vec4f &V1, const Vec4f &V2);

Vec4f operator*(float f, const Vec4f &V);	Vec4f operator*(const Vec4f &V, float f);
Vec4f operator*(int i, const Vec4f &V);		Vec4f operator*(const Vec4f &V, int i);
Vec4f operator/(const Vec4f &V, float f);
Vec4f operator-(const Vec4f &V1);

std::ostream& operator<<(std::ostream& os, const Vec4f &V);
std::istream& operator>>(std::istream& is, Vec4f &V);

///Vec3f
bool  operator==(const Vec3f &V1, const Vec3f &V2);
Vec3f operator*(const Vec3f &V1, const Vec3f &V2);
Vec3f operator+(const Vec3f &V1, const Vec3f &V2);
Vec3f operator-(const Vec3f &V1, const Vec3f &V2);
void  operator*=(Vec3f &V1, const Vec3f &V2);
void  operator+=(Vec3f &V1, const Vec3f &V2);
void  operator-=(Vec3f &V1, const Vec3f &V2);

Vec3f operator*(float f, const Vec3f &V);	Vec3f operator*(const Vec3f &V, float f);
Vec3f operator*(int i, const Vec3f &V);		Vec3f operator*(const Vec3f &V, int i);
Vec3f operator*(uint32_t i, const Vec3f &V);		Vec3f operator*(const Vec3f &V, uint32_t i);
Vec3f operator/(const Vec3f &V, float f);
Vec3f operator-(const Vec3f &V1);


std::ostream& operator<<(std::ostream& os, const Vec3f &V);
std::istream& operator>>(std::istream& is, Vec3f &V);

///Vec3i
bool  operator==(const Vec3i &V1, const Vec3i &V2);
Vec3i operator*(const Vec3i &V1, const Vec3i &V2);
Vec3i operator+(const Vec3i &V1, const Vec3i &V2);
Vec3i operator-(const Vec3i &V1, const Vec3i &V2);
void  operator*=(Vec3i &V1, const Vec3i &V2);
void  operator+=(Vec3i &V1, const Vec3i &V2);
void  operator-=(Vec3i &V1, const Vec3i &V2);

Vec3f operator*(float f, const Vec3i &V);	Vec3f operator*(const Vec3i &V, float f);
Vec3i operator*(int i, const Vec3i &V);		Vec3i operator*(const Vec3i &V, int i);
Vec3f operator/(const Vec3i &V, float f);
Vec3i operator-(const Vec3i &V1);

std::ostream& operator<<(std::ostream& os, const Vec3i &V);
std::istream& operator>>(std::istream& is, Vec3i &V);

///Vec2f
bool  operator==(const Vec2f &V1, const Vec2f &V2);
Vec2f operator*(const Vec2f &V1, const Vec2f &V2);
Vec2f operator+(const Vec2f &V1, const Vec2f &V2);
Vec2f operator-(const Vec2f &V1, const Vec2f &V2);
void  operator*=(Vec2f &V1, const Vec2f &V2);
void  operator+=(Vec2f &V1, const Vec2f &V2);
void  operator-=(Vec2f &V1, const Vec2f &V2);

Vec2f operator*(const Vec2f &V1, const Vec2i &V2);	Vec2f operator*(const Vec2i &V1, const Vec2f &V2);

Vec2f operator*(float f, const Vec2f &V);	Vec2f operator*(const Vec2f &V, float f);
Vec2f operator*(int i, const Vec2f &V);		Vec2f operator*(const Vec2f &V, int i);
Vec2f operator/(const Vec2f &V, float f);
Vec2f operator-(const Vec2f &V1);

std::ostream& operator<<(std::ostream& os, const Vec2f &V);
std::istream& operator>>(std::istream& is, Vec2f &V);

inline Vec2f Rotate90CW(Vec2f V)	{return(Vec2f{V.y, -V.x});};
inline Vec2f Rotate90CCW(Vec2f V)	{return(Vec2f{-V.y, V.x});};

///Vec2i
bool  operator==(const Vec2i &V1, const Vec2i &V2);
Vec2i operator*(const Vec2i &V1, const Vec2i &V2);
Vec2i operator+(const Vec2i &V1, const Vec2i &V2);
Vec2i operator-(const Vec2i &V1, const Vec2i &V2);
void  operator*=(Vec2i &V1, const Vec2i &V2);
void  operator+=(Vec2i &V1, const Vec2i &V2);
void  operator-=(Vec2i &V1, const Vec2i &V2);

Vec2f operator*(float f, const Vec2i &V);	Vec2f operator*(const Vec2i &V, float f);
Vec2i operator*(int i, const Vec2i &V);		Vec2i operator*(const Vec2i &V, int i);
Vec2f operator/(const Vec2i &V, float f);
Vec2i operator-(const Vec2i &V1);

std::ostream& operator<<(std::ostream& os, const Vec2i &V);
std::istream& operator>>(std::istream& is, Vec2i &V);