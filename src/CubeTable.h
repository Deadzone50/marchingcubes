#pragma once
#include <vector>

class CubeTable
{
public:
	CubeTable();
	std::vector<uint8_t> GetCube(bool Corners[8]);
	void AddInvert(const std::vector<uint8_t> &V, const std::vector<uint8_t> &Indices, bool Invert);
	void AddRotateX(const std::vector<uint8_t> &V, const std::vector<uint8_t> &Indices, bool Invert);
	void AddRotateY(const std::vector<uint8_t> &V, const std::vector<uint8_t> &Indices, bool Invert);
	void AddRotateZ(const std::vector<uint8_t> &V, const std::vector<uint8_t> &Indices, bool Invert);
	void AddAllConfigurations(const std::vector<uint8_t> &V, const std::vector<uint8_t> &Indices, bool Invert = false);

	std::vector<uint8_t> Table_[256];
};
