#include "NavMesh.h"

inline bool PositionCompare(const Vec3f &V1, const Vec3f &V2)
{
	const float delta = 0.001f;
	return ((V2.x - delta < V1.x && V1.x < V2.x + delta) &&
			(V2.y - delta < V1.y && V1.y < V2.y + delta) &&
			(V2.z - delta < V1.z && V1.z < V2.z + delta));
}

//bool NavZone::AppendIfBordering(Vec3f Trig[3])
//{
//	uint32_t NumCorners = m_Corners.size();
//	for(uint32_t i = 0; i < NumCorners; ++i)
//	{
//		Vec3f PrevCorner;
//		if(i > 0)
//			PrevCorner = m_Corners[i-1];
//		else
//			PrevCorner = m_Corners[NumCorners-1];
//
//		Vec3f CurrentCorner = m_Corners[i];
//		Vec3f NextCorner = m_Corners[(i+1)%NumCorners];
//
//		if(PositionCompare(CurrentCorner, Trig[0]))
//		{
//			if(PositionCompare(NextCorner, Trig[2]))
//			{
//				m_Corners.insert(m_Corners.begin() +i+1, Trig[1]);
//				return true;
//			}
//		}
//		if(PositionCompare(CurrentCorner, Trig[1]))
//		{
//			if(PositionCompare(NextCorner, Trig[0]))
//			{
//				m_Corners.insert(m_Corners.begin() +i+1, Trig[2]);
//				return true;
//			}
//		}
//		if(PositionCompare(CurrentCorner, Trig[2]))
//		{
//			if(PositionCompare(NextCorner, Trig[1]))
//			{
//				m_Corners.insert(m_Corners.begin() +i+1, Trig[0]);
//				return true;
//			}
//		}
//	}
//	return false;
//}

bool CheckDuplicateEdge(NavPoint *NP, Edge *Edge)
{
	for(auto &E : NP->Edges)
	{
		if((E.N1 == Edge->N1 && E.N2 == Edge->N2) ||
		   (E.N2 == Edge->N1 && E.N1 == Edge->N2))
		{
			return true;
		}
	}
	return false;
}

void NavMesh::AddTriangle(Vec3f Corners[3])
{
	//for(auto &NZ : m_NavZones)
	//{
	//	if(NZ.AppendIfBordering(Corners))
	//	{
	//		return;
	//	}
	//}
	//NavZone NZ;
	//NZ.m_Corners.push_back(Corners[0]);
	//NZ.m_Corners.push_back(Corners[1]);
	//NZ.m_Corners.push_back(Corners[2]);
	//m_NavZones.push_back(NZ);

	NavPoint *NP0 = NULL;
	NavPoint *NP1 = NULL;
	NavPoint *NP2 = NULL;
	for(uint32_t i = 0; i < m_Points.size(); ++i)
	{
		if(PositionCompare(Corners[0], m_Points[i]->Pos))
		{
			NP0 = m_Points[i];
		}
		if(PositionCompare(Corners[1], m_Points[i]->Pos))
		{
			NP1 = m_Points[i];
		}
		if(PositionCompare(Corners[2], m_Points[i]->Pos))
		{
			NP2 = m_Points[i];
		}
		if(NP0 && NP1 && NP2)
		{
			break;
		}
	}
	if(NP0 == NULL)
	{
		NP0 = new NavPoint{Corners[0]};
		m_Points.push_back(NP0);
	}
	if(NP1 == NULL)
	{
		NP1 = new NavPoint{Corners[1]};
		m_Points.push_back(NP1);
	}
	if(NP2 == NULL)
	{
		NP2 = new NavPoint{Corners[2]};
		m_Points.push_back(NP2);
	}
	Edge E01 = {NP0, NP1};
	if(!CheckDuplicateEdge(NP0, &E01))	NP0->Edges.push_back(E01);
	if(!CheckDuplicateEdge(NP1, &E01))	NP1->Edges.push_back(E01);
	Edge E12 = {NP1, NP2};
	if(!CheckDuplicateEdge(NP1, &E12))	NP1->Edges.push_back(E12);
	if(!CheckDuplicateEdge(NP2, &E12))	NP2->Edges.push_back(E12);
	Edge E02 = {NP0, NP2};
	if(!CheckDuplicateEdge(NP0, &E02))	NP0->Edges.push_back(E02);
	if(!CheckDuplicateEdge(NP2, &E02))	NP2->Edges.push_back(E02);
}

void NavMesh::Generate()
{
	NavZone NZ;

	std::vector<NavPoint*> PointsLeft = m_Points;

	std::vector<NavPoint*> ZonePoints;
	std::vector<NavPoint*> ToProcess;
	
	ToProcess.push_back(PointsLeft.back());
	PointsLeft.pop_back();

	while(!ToProcess.empty())
	{
		NavPoint *CurrentP = ToProcess.back();
		ToProcess.pop_back();
		ZonePoints.push_back(CurrentP);
		for(auto &E : CurrentP->Edges)
		{
			NavPoint *TP;
			if(E.N1 == CurrentP)
				TP = E.N2;
			else
				TP = E.N1;
			ToProcess.push_back(TP);
		}
		if(CurrentP->Edges.size() == 2)
		{
			for(auto &E : CurrentP->Edges)
			{
				E.Marked = true;
			}
		}
		else
		{
			for(auto &E : CurrentP->Edges)
			{
				E.Marked = true;
			}
		}
	}
	
	void;
}
